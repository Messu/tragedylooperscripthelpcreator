
existingScenarioList = existingScenarioList.concat([
    {
        "tragedySet": "first_steps",
        "title": "Love, or a Nightmare?",
        "origine_id": "159",
        "origine_tag": "TLS",
        "origine": "Tragedylooperscripts.com",
        "informations": "<p>\n<strong>Créateur</strong> : CipherCybr<br />\n<strong>Boucles</strong> : 4 / <strong>Difficulté</strong> : Moderate<br />\n<strong>Jours par boucle</strong> : 4\n</p>\n<h4>Histoire</h4>\n<p><strong style=\"color:red;\">Ce script contient le personnage Être divin qui arrive à partir d'une certaine boucle. Les Protagonistes n'apprennent son existence qu'à son arrivée sur le plateau, pas avant.</strong></p><p>\nWhispers of an ancient evil emanate from the Shrine, the stars are aligned, the fateful day approaches. Echoes of fear manifest in those vulnerable minds, manifesting in panic, while a vessel of the Dark Ones arrives in order to ensure their coming is set in stone. Meanwhile, a love triangle has developed in the school, centering around the mysterious new student. However, he is far from what he seems, and the battleground of love forming around him may brew desperate acts with fatal consequences. Can the Protagonists save the students from one another and protect the world from the darkness that claws at its seams?\n</p>\n<h4>Conseils pour le Mastermind</h4>\n<p>\nRecommended for experienced Protagonists and Masterminds that are not yet ready for Basic Tragedy. This script heavily relies on the Mastermind's ability to cover-up and bluff, as there are only two ways for the Mastermind to win, and both can be blocked relatively easily by the Protagonists. You do have some tools at your disposal, however. -The Informer and Office Worker are good early game bait for the Protagonists to waste time on. Use them either as decoys or false leads as you play it out. -Be aggressive in using the abilities at your disposal in the beginning. Incidents are very important in this script, particularly on the first day. Use the combined abilities of your cards, the Conspiracy Theorist, and the Curmudgeon Doctor to stack up paranoia and keep the Protagonists guessing as to the culprits of the incidents. -Between placing 2 Intrigue on the School and placing 2 Intrigue on the Girl Student (Friend), the Cultist's presence will quickly become apparent, which you can use to your advantage when covering up the main plot. However, if the Protagonists discover that the Mystery Boy is the Cultist, it will destroy all cover-ups related to A Place to Protect. Be prepared for this. -Once the Godly Being arrives in Loop 3, how you want to use it will depend on how close the Protagonists are to figuring your Plots out. If they are near certain as to what they are, go loud with its Brain ability. You should be able to secure the win and prepare for the battle that is the last loop. Alternatively, if they are still unsure, use it only to secure the win, and preferably when there's room to deceive. Be aware that particularly experienced Protagonists may predict the arrival of the Godly Being if you focus on placing Intrigue on the Shrine.\n</p>",
        "mastermind_victory_conditions": "<ol>\n    <li>\n        <strong>Tuer l'Ami (Lycéenne)</strong><br/>\n        L'incident Meutre distant\n    </li>\n    <li>\n        <strong>Avoir 2 Intrigues ou plus sur le Sanctuaire</strong><br/>\n        Machination principale : Lumière vengeresse\n    </li>\n</ol>",
        "machination_primary": "lumiere_vengeresse",
        "machination_secondary_list": [
            "un_script_terrifiant"
        ],
        "character_list": [
            {
                "characterCode": "patient",
                "roleCode": ""
            },
            {
                "characterCode": "medecin",
                "roleCode": "grincheux"
            },
            {
                "characterCode": "employe_de_bureau",
                "roleCode": ""
            },
            {
                "characterCode": "deleguee_de_classe",
                "roleCode": "conspirationniste"
            },
            {
                "characterCode": "garcon_mysterieux",
                "roleCode": "cultiste"
            },
            {
                "characterCode": "lyceenne",
                "roleCode": "ami"
            },
            {
                "characterCode": "informatrice",
                "roleCode": ""
            },
            {
                "characterCode": "etre_divin",
                "roleCode": "cerveau",
                "additionnal_field": "Entre en jeu à la boucle 3"
            }
        ],
        "incident_list": [
            {
                "incidentCode": "malaise_grandissant",
                "jourNumero": "1",
                "coupable": "deleguee_de_classe"
            },
            {
                "incidentCode": "personne_disparue",
                "jourNumero": "2",
                "coupable": "employe_de_bureau"
            },
            {
                "incidentCode": "meurtre_distant",
                "jourNumero": "4",
                "coupable": "patient"
            }
        ]
    },
    {
        "tragedySet": "first_steps",
        "title": "Those from Beyond",
        "origine_id": "164",
        "origine_tag": "TLS",
        "origine": "Tragedylooperscripts.com",
        "informations": "<p>\n<strong>Créateur</strong> : CipherCybr<br />\n<strong>Boucles</strong> : 3 / <strong>Difficulté</strong> : Moderate<br />\n<strong>Jours par boucle</strong> : 4\n</p>\n<h4>Particularités</h4>\n<p>\n<strong>IMPORTANT</strong>: You need the expansion character Illusion to play this script.\n</p>\n<h4>Histoire</h4>\n<p>\nFrom the dark they come, living shadows pressed into human form, their hunger for living souls insatiable. The brother, a former forerunner now living life peacefully as a human, and the sister, bloodthirsty and seeking vengeance for being blocked from our world by her sibling. With her summoning by an unwitting shrine maiden, tendrils flow outward from that holy place, touching many with a desire to release the essence of the living for her consumption and empowerment, in the only way they know how: death and desecration. \n</p>\n<h4>Conseils pour le Mastermind</h4>\n<p>\nA tricky script with an overarching win condition, but with a few nasty surprises for the Protagonists to disrupt certain strategies. Your main focus will be intrigue on the Shrine: once that is achieved, damage control to prevent any more information falling into the hands of the Protagonists is key. It might be tempting to have the Mystery Boy (Key Person) killed by the Illusion (Serial Killer) for a quick loop end, however it will reveal the main plot and the role of a wildcard character, so it is best saved for the last loop. One potential early strategy is to move another character into the Shrine and then pump it with Intrigue via the Brain and your cards, to hide both the Brain and the Serial Killer while guaranteeing an early loop win. You should save Incidents for Loop 2, with Missing Person able to add 1 Intrigue to the Shrine, and both the Suicide and Murder able to kill the Key Person as a last resort. You should try to get the Doctor killed if you can, as his Goodwill ability will either force you to block it every turn, or render the Patient's Conspiracy Theorist ability useless.",
        "mastermind_victory_conditions": "<ol>\n    <li>\n        <strong>Tuer le Personnage clé (Garçon mystérieux)</strong><br/>\n        La capacité du Tueur en série / l'incident Meutre\n    </li>\n    <li>\n        <strong>Avoir 2 Intrigues ou plus sur le Sanctuaire</strong><br/>\n        Machination principale : Lumière vengeresse<br/>\n        Capacité du Cerveau/Cartes Intrigue/Incident Personne disparue\n    </li>\n</ol>",
        "machination_primary": "lumiere_vengeresse",
        "machination_secondary_list": [
            "l_ombre_de_leventreur"
        ],
        "character_list": [
            {
                "characterCode": "medecin",
                "roleCode": ""
            },
            {
                "characterCode": "patient",
                "roleCode": "conspirationniste"
            },
            {
                "characterCode": "gardienne_du_sanctuaire",
                "roleCode": "cerveau"
            },
            {
                "characterCode": "illusion",
                "roleCode": "tueur_en_serie"
            },
            {
                "characterCode": "employe_de_bureau",
                "roleCode": ""
            },
            {
                "characterCode": "garcon_mysterieux",
                "roleCode": "personnage_cle"
            },
            {
                "characterCode": "deleguee_de_classe",
                "roleCode": ""
            }
        ],
        "incident_list": [
            {
                "incidentCode": "meurtre",
                "jourNumero": "2",
                "coupable": "patient"
            },
            {
                "incidentCode": "personne_disparue",
                "jourNumero": "3",
                "coupable": "employe_de_bureau"
            },
            {
                "incidentCode": "suicide",
                "jourNumero": "4",
                "coupable": "garcon_mysterieux"
            }
        ]
    },
    {
        "tragedySet": "first_steps",
        "title": "United We Fall",
        "origine_id": "163",
        "origine_tag": "TLS",
        "origine": "Tragedylooperscripts.com",
        "informations": "<p>\n<strong>Créateur</strong> : ChrisMaxheart<br />\n<strong>Boucles</strong> : 2 or 3 or 4 / <strong>Difficulté</strong> : Easy<br />\n<strong>Jours par boucle</strong> : 4\n</p>\n<h4>Règle spéciale</h4>\n<p>\nLa Déduction finale est autorisée.\n</p>\n<h4>Description</h4>\n<p>\nAlternative first script.\n</p>",
        "mastermind_victory_conditions": "<ol>\n    <li>\n        <strong>Tuer le Personnage clé (Lycéenne)</strong><br/>\n        La capacité du Tueur/La capacité du Tueur en série / l'incident Meutre / l'incident Suicide\n    </li>\n</ol>",
        "machination_primary": "meurtre_premedite",
        "machination_secondary_list": [
            "l_ombre_de_leventreur"
        ],
        "character_list": [
            {
                "characterCode": "lyceenne",
                "roleCode": "personnage_cle"
            },
            {
                "characterCode": "gardienne_du_sanctuaire",
                "roleCode": "tueur"
            },
            {
                "characterCode": "riche_heritiere",
                "roleCode": "cerveau"
            },
            {
                "characterCode": "lyceen",
                "roleCode": "conspirationniste"
            },
            {
                "characterCode": "employe_de_bureau",
                "roleCode": "tueur_en_serie"
            },
            {
                "characterCode": "infirmiere",
                "roleCode": ""
            }
        ],
        "incident_list": [
            {
                "incidentCode": "malaise_grandissant",
                "jourNumero": "2",
                "coupable": "riche_heritiere"
            },
            {
                "incidentCode": "meurtre",
                "jourNumero": "3",
                "coupable": "lyceen"
            },
            {
                "incidentCode": "suicide",
                "jourNumero": "4",
                "coupable": "lyceenne"
            }
        ]
    },
    {
        "tragedySet": "first_steps",
        "title": "The World Evervolving",
        "origine_id": "162",
        "origine_tag": "TLS",
        "origine": "Tragedylooperscripts.com",
        "informations": "<p>\n<strong>Créateur</strong> : CipherCybr<br />\n<strong>Boucles</strong> : 4 / <strong>Difficulté</strong> : Very Hard<br />\n<strong>Jours par boucle</strong> : 4\n</p>\n<h4>Règle spéciale (Info pour les Protagonistes)</h4>\n<p>\n[At Day End]: Move the Shrine anti-clockwise on the board and switch it with the Location in the slot it moves to (Example: Night 1 - Switch the Shrine and Hospital). Then, an effect (only known by Mastermind) is applied to all characters in that location.\n</p>\n<h4>Règle spéciale (Info pour le Mastermind)</h4>\n<p>\nDay 1: Place 1 Paranoia on all characters in the Shrine.<br/>\nDay 2: Replace all Paranoia on all characters in the Shrine with Goodwill, and place 1 Goodwill on each character that had Paranoia replaced.<br/>\nDay 3: Replace all Goodwill on all characters in the Shrine with Intrigue, and place 1 Intrigue on each character that had Goodwill replaced.<br/>\nDay 4: All characters with at least 2 Intrigue in the Shrine die.\n</p>\n<h4>Histoire</h4>\n<p>\nShe was the key. At least, the humble maiden of the shrine was made the key to this Pandora's Box. From anywhere else, it looked like a regular place of worship. Step within the sacred grounds, however, and a world twisting and contorting would greet the eyes of the unwary. Having discovered this phenomenon, the Protagonists must work with and protect a woman interested in the shifting shrine, alongside the key, lest the rotations spread beyond the shrine.\n</p>\n<h4>Conseils pour le Mastermind</h4>\n<p>\nFirst of all, kudos to those that get the reference. This script is as much chaos for the Mastermind as it is for the Protagonists. You should aim to get the Friend killed on Loop 1 and 2 (Special Rule and Faraway Murder), and try to save the Key Person's death and the A Place to Protect loss condition for the last two loops. Be very aware of how the board will change with the moving Shrine, and be especially careful of movement restrictions on certain characters. One way to quickly kill someone is to place +2 Intrigue on someone and then send them to the Shrine, but the Protagonists will not fall for this twice. It is possible for the Protagonists to exploit the Special Rule on Day 2 to gain more Goodwill on specific characters, so be prepared to counter this. With so much potential Paranoia on the board, it can be fairly easy to cover up the culprits of Incidents in the first couple of loops. On the final loop, aim to trigger Missing Person and move the Shrine Maiden to the School, and take advantage of the age old catch 22: +1 Intrigue on the School, or +2 somewhere worse. The key to victory here is to take advantage of how much you can bamboozle the Protagonists with an ever-shifting board and a terrifying amount of Paranoia and Intrigue generation.\n</p>",
        "mastermind_victory_conditions": "<ol>\n    <li>\n        <strong>Tuer l'Ami (Gardienne du Sanctuaire)</strong><br/>\n        L'incident Meutre distant/Règle spéciale\n    </li>\n    <li>\n        <strong>Tuer le Personnage clé (Informatrice)</strong><br/>\n        L'incident Meutre distant/Règle spéciale\n    </li>\n    <li>\n        <strong>Avoir 2 Intrigues ou plus sur le Lycée</strong><br/>\n        Machination principale : un lieu à protéger\n    </li>\n</ol>",
        "machination_primary": "un_lieu_a_proteger",
        "machination_secondary_list": [
            "un_script_terrifiant"
        ],
        "character_list": [
            {
                "characterCode": "gardienne_du_sanctuaire",
                "roleCode": "ami"
            },
            {
                "characterCode": "lyceen",
                "roleCode": "conspirationniste"
            },
            {
                "characterCode": "lyceenne",
                "roleCode": ""
            },
            {
                "characterCode": "riche_heritiere",
                "roleCode": ""
            },
            {
                "characterCode": "employe_de_bureau",
                "roleCode": "cultiste"
            },
            {
                "characterCode": "informatrice",
                "roleCode": "personnage_cle"
            },
            {
                "characterCode": "medecin",
                "roleCode": "grincheux"
            },
            {
                "characterCode": "infirmiere",
                "roleCode": ""
            }
        ],
        "incident_list": [
            {
                "incidentCode": "malaise_grandissant",
                "jourNumero": "1",
                "coupable": "medecin"
            },
            {
                "incidentCode": "personne_disparue",
                "jourNumero": "3",
                "coupable": "gardienne_du_sanctuaire"
            },
            {
                "incidentCode": "meurtre_distant",
                "jourNumero": "4",
                "coupable": "employe_de_bureau"
            }
        ]
    },
    {
        "tragedySet": "first_steps",
        "title": "A Decadent Rebellion",
        "origine_id": "161",
        "origine_tag": "TLS",
        "origine": "Tragedylooperscripts.com",
        "informations": "<p>\n<strong>Créateur</strong> : CipherCybr<br />\n<strong>Boucles</strong> : 4 / <strong>Difficulté</strong> : Hard<br />\n<strong>Jours par boucle</strong> : 4\n</p>\n<h4>Règle spéciale</h4>\n<p>\n[Always]: Characters with Boy or Man cannot move into the School by any means.\n</p>\n<h4>Histoire</h4>\n<p>\n Young love often signaled a turbulent time of passion and change. Unfortunately for this city, when the boy became a patient at a mental ward, it also signaled the birth of a terrifying plan to elope, and the only thing in the lovers' way? The very nurse that had admitted him. And with the charm and resources of a very successful Pop Idol enrolled in an all-girls school? \"Collateral damage\" is all but certain, should the dark pair successfully enact their plan. But they are not outside of the public eye. A humble man of nine-to-fives is aware of the danger they pose, and must keep his secret, lest the lovers force him to take it to the grave.\n</p>\n<h4>Conseils pour le Mastermind</h4>\n<p>\nThis is a very tricky First Steps script for Protagonist and Mastermind alike. You have a lot of tools to achieve your victory each loop, and there are several big threats that will draw the attention of the Protagonists very early on. However, your job is far from easy. While you are able to pull off some interesting plays off the special rule, including preventing movement and forcing the Protagonists into a catch-22, the School is a safe zone against some of your plays, especially if the Pop Idol ends up there. There are also 3 potentially useful Goodwill 2 abilities, and none of them have Goodwill Refusal, so expect these to be used to full effect, on top of the Friend (Office Worker) potentially being revealed before you can win using them. Finally, it is highly likely that the main plot will be discovered quickly, so you must take every opportunity to conceal the identities of your Key Person (Nurse) and Killer (Pop Idol) during every loop but the last. Your most common route to victory early on will involve the Hospital Incident, using the Conspiracy Theorist as the culprit and your cards, you can easily pull off 2 Paranoia on the Patient. Between killing the Friend via the Murder incident and a Intrigue turbo combo (Day 1: +2 Intrigue on Killer, then +1 Intrigue from Brain), you also have a couple of backup plans in case things go awry. If the Protagonists' strategy relies on a specific Goodwill ability (notably the Nurse or Class Rep), you can have the Henchman steal 2 Goodwill from that character using the Spreading incident, placing it on a useless Goodwill ability (as his incident has already triggered).\n</p>",
        "mastermind_victory_conditions": "<ol>\n    <li>\n        <strong>Tuer le Personnage clé (Infirmière)</strong><br/>\n        La capacité du Tueur/l'incident Meutre/Incident à l'Hôpital\n    </li>\n    <li>\n        <strong>Tuer l'Ami (Lycéenne)</strong><br/>\n        L'incident Meutre/Incident à l'Hôpital\n    </li>\n    <li>\n        <strong>Tuer les Protagonistes</strong><br/>\n        L'incident Meutre/Incident à l'Hôpital\n    </li>\n</ol>",
        "machination_primary": "meurtre_premedite",
        "machination_secondary_list": [
            "un_script_terrifiant"
        ],
        "character_list": [
            {
                "characterCode": "patient",
                "roleCode": "conspirationniste"
            },
            {
                "characterCode": "infirmiere",
                "roleCode": "personnage_cle"
            },
            {
                "characterCode": "sbire",
                "roleCode": "cerveau"
            },
            {
                "characterCode": "pop_star",
                "roleCode": "tueur"
            },
            {
                "characterCode": "policier",
                "roleCode": ""
            },
            {
                "characterCode": "lyceenne",
                "roleCode": "ami"
            },
            {
                "characterCode": "gardienne_du_sanctuaire",
                "roleCode": ""
            },
            {
                "characterCode": "deleguee_de_classe",
                "roleCode": ""
            }
        ],
        "incident_list": [
            {
                "incidentCode": "contagion",
                "jourNumero": "2",
                "coupable": "sbire"
            },
            {
                "incidentCode": "meurtre",
                "jourNumero": "3",
                "coupable": "pop_star"
            },
            {
                "incidentCode": "incident_a_l_hopital",
                "jourNumero": "4",
                "coupable": "patient"
            }
        ]
    },
    {
        "tragedySet": "first_steps",
        "title": "A Cruel Shrine Maiden's Thesis",
        "origine_id": "153",
        "origine_tag": "TLS",
        "origine": "Tragedylooperscripts.com",
        "informations": "<p>\n<strong>Créateur</strong> :  Redless<br />\n<strong>Boucles</strong> : 3 ou 4 / <strong>Difficulté</strong> : 7/10<br />\n<strong>Jours par boucle</strong> : 4\n</p>\n<h4>Histoire</h4>\n<p>\nThe Shrine Maiden loves to end the loop on the first day by serial killing the key person in the shrine. It's hard for her to make time in her busy schedule to do this, but tonight she's ready to indulge herself. I hope you like trolling your players by making them deal with this a second time.\n</p>\n<h4>Conseils pour le Mastermind</h4>\n<p>\nThis whole script revolves around missing person. It will let you drag the nurse into the maiden's path, and also let you stack some more intrigue on the school. It's your \"easy out\", everything else in the script lives in service of it. The first day you should aim to serial kill the nurse. LOL! Gotem. Another loop, drop paranoia on the OW, and move the girl student left. By doing this, you will be in a good spot to MP. If people don't die, stack the school and set to work adding intrigue. If people do die, try to position the shrine maiden alone so that you can MP the nurse over. Another loop, try and stack people in the school and obfuscate who exactly the cultist is. Finally, try and win the final loop via missing person shenanigans, which is surprisingly hard to do. You might consider playing this instead of the first script, or in place of the second script if your players don't mind you crushing their spirits by doing the same BS strat that characterizes the first script, a second time in a row. I believe this is very balanced. But of course you have to contend with the repetition of serial killer if you're playing this after the first script.\n</p>",
        "mastermind_victory_conditions": "<p>\nSerial Kill Nurse or School Intrigue (Pop Idol can help). You also have emergency button missingperson to fall back on.\n</p>",
        "machination_primary": "un_lieu_a_proteger",
        "machination_secondary_list": [
            "l_ombre_de_leventreur"
        ],
        "character_list": [
            {
                "characterCode": "gardienne_du_sanctuaire",
                "roleCode": "tueur_en_serie"
            },
            {
                "characterCode": "deleguee_de_classe",
                "roleCode": ""
            },
            {
                "characterCode": "lyceenne",
                "roleCode": "conspirationniste"
            },
            {
                "characterCode": "infirmiere",
                "roleCode": "personnage_cle"
            },
            {
                "characterCode": "patient",
                "roleCode": ""
            },
            {
                "characterCode": "pop_star",
                "roleCode": "cultiste"
            },
            {
                "characterCode": "employe_de_bureau",
                "roleCode": ""
            }
        ],
        "incident_list": [
            {
                "incidentCode": "malaise_grandissant",
                "jourNumero": "1",
                "coupable": "employe_de_bureau"
            },
            {
                "incidentCode": "personne_disparue",
                "jourNumero": "4",
                "coupable": "infirmiere"
            }
        ]
    },
    {
        "tragedySet": "first_steps",
        "title": "An old friend",
        "origine_id": "120",
        "origine_tag": "TLS",
        "origine": "Tragedylooperscripts.com",
        "informations": "<p>\n<strong>Créateur</strong> : Diego Whitetower<br />\n<strong>Posté par</strong> :  Lethan<br />\n<strong>Boucles</strong> : 4 / <strong>Difficulté</strong> : 2<br />\n<strong>Boucles</strong> : 3 / <strong>Difficulté</strong> : 4<br />\n<strong>Jours par boucle</strong> : 4\n</p>\n<h4>Histoire</h4>\n<p>\nYou have received a text from an old childhood friend. She's discovered that another student in her school is a drug dealer, and now she's in trouble and your help to survive.\n</p>\n<h4>Conseils pour le Mastermind</h4>\n<p>\nAlways play a card on the class Rep in the first day, they won't know if it is paranoia or intrigue. Try to murder her in the second loop, but place at least 1 intrigue counter on the school, just in case. Try to place 2 intrigue counters in other place to misdirect the players. In the third loop, go for the victory condition you haven't achieve in previous loops.\n</p>",
        "mastermind_victory_conditions": "<p>\nThe friend is dead at the end of the loop: you can do this by the suicide or the murder. There is 2 Intrigue counters on the school at the end of the loop. You can use the Brain ability and the missing person incident.\n</p>",
        "machination_primary": "lumiere_vengeresse",
        "machination_secondary_list": [
            "un_script_terrifiant"
        ],
        "character_list": [
            {
                "characterCode": "lyceenne",
                "roleCode": "cerveau"
            },
            {
                "characterCode": "lyceen",
                "roleCode": "conspirationniste"
            },
            {
                "characterCode": "deleguee_de_classe",
                "roleCode": "ami"
            },
            {
                "characterCode": "policier",
                "roleCode": "grincheux"
            },
            {
                "characterCode": "medecin",
                "roleCode": "grincheux"
            },
            {
                "characterCode": "pop_star",
                "roleCode": ""
            },
            {
                "characterCode": "gardienne_du_sanctuaire",
                "roleCode": ""
            }
        ],
        "incident_list": [
            {
                "incidentCode": "suicide",
                "jourNumero": "1",
                "coupable": "deleguee_de_classe"
            },
            {
                "incidentCode": "personne_disparue",
                "jourNumero": "3",
                "coupable": "policier"
            },
            {
                "incidentCode": "meurtre",
                "jourNumero": "4",
                "coupable": "medecin"
            }
        ]
    },
    {
        "tragedySet": "base_tragedy",
        "title": "The Outsider's Wish",
        "origine_id": "177",
        "origine_tag": "TLS",
        "origine": "Tragedylooperscripts.com",
        "informations": "<p>\n<strong>Créateur</strong> : Chronicle [RealBernkastel]<br />\n<strong>Boucles</strong> : 4 / <strong>Difficulté</strong> : Modérée<br />\n<strong>Jours par boucle</strong> : 5\n</p>\n<h4>Histoire</h4>\n<p>\nA Police Officer and his co-worker arrive at the City after receiving a call from the School Teacher that there's been an incident which; while now, subsided, still requires looking into. The victim of the incident was brutally injured after falling from a flight of stairs; of which they claim was not accidental. While the Patient is now being looked after in the Hospital, the Police Officer's co-worker is not sure of this. A worrisome lad, he's always been. After all, they're just students; what's the worse that could brew? A Tragedy? Pfft~\n</p>\n<h4>Conseils pour le Mastermind</h4>\n<p>\nThis script is very flexible and can be played in a multitude of ways; all of which involve killing a SINGLE Friend and keeping the Key Person alive until necessary. Due to this, a general hint would be to try and hide the actual Serial Killer which might not be easy depending on the protagonists; and using the Class Rep as a decoy Serial Killer. On Day 3, Triggering the Murder Incident is paramount if you were not able to kill the Teacher prior to this day. The Faraway Murder can be used in a plethora of ways; and it is up to you to figure out the best way at that current situation. Try to avoid the Suicide Incident. You do NOT want to trigger it. Protagonists might be able to use this to their advantage.\n</p>",
        "mastermind_victory_conditions": "<ol>\n    <li>\n        <strong>Tuer le Personnage clé (Police Officer)</strong>\n    </li>\n    <li>\n        <strong>Tuer au moins UN ami (Infirmière/Enseignante)</strong>\n    </li>\n</ol>",
        "machination_primary": "meurtre_premedite",
        "machination_secondary_list": [
            "fils_du_destin",
            "cercle_d_amis"
        ],
        "character_list": [
            {
                "characterCode": "employe_de_bureau",
                "roleCode": "conspirationniste"
            },
            {
                "characterCode": "lyceen",
                "roleCode": "cerveau"
            },
            {
                "characterCode": "riche_heritiere",
                "roleCode": "tueur"
            },
            {
                "characterCode": "enseignante",
                "roleCode": "ami"
            },
            {
                "characterCode": "infirmiere",
                "roleCode": "ami"
            },
            {
                "characterCode": "policier",
                "roleCode": "personnage_cle"
            },
            {
                "characterCode": "patient",
                "roleCode": ""
            },
            {
                "characterCode": "deleguee_de_classe",
                "roleCode": ""
            }
        ],
        "incident_list": [
            {
                "incidentCode": "mal_odieux",
                "jourNumero": "1",
                "coupable": "enseignante"
            },
            {
                "incidentCode": "suicide",
                "jourNumero": "2",
                "coupable": "riche_heritiere"
            },
            {
                "incidentCode": "meurtre",
                "jourNumero": "3",
                "coupable": "patient"
            },
            {
                "incidentCode": "meurtre_distant",
                "jourNumero": "4",
                "coupable": "policier"
            },
            {
                "incidentCode": "meurtre",
                "jourNumero": "5",
                "coupable": "deleguee_de_classe"
            }
        ]
    }
]);
