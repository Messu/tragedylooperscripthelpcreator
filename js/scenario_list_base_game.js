existingScenarioList = existingScenarioList.concat([
    {
        "tragedySet": "first_steps",
        "title": "Le premier script",
        "origine_id": "1",
        "origine_tag": "BB",
        "origine": "Boîte de base",
        "informations": "<p>\n<strong>Créateur</strong> : BakaFire<br />\n<strong>Boucles</strong> : 3 / <strong>Difficulté</strong> : 1<br />\n<strong>Boucles</strong> : 2 / <strong>Difficulté</strong> : 6<br />\n<strong>Jours par boucle</strong> : 4\n</p>\n<h4>Particularités</h4>\n<p>\nLe premier script est conçu pour être joué juste après l'explication des règles, quand il s'agit de la première partie, tant pour le Mastermind que pour les Protagonistes. Vous pouvez ainsi y jouer relativement décontracté en vous concentrant principalement sur la mécanique.\n</p>\n<p>\nEn tant que script d'entrainement, il n'est pas très difficile. Ne vous préoccupez pas ici de perdre ou de gagner, prenez-le tout simplement comme une session d'entrainement.\n</p>\n<h4>Histoire</h4>\n<p>\nUne fille a découvert un secret. pour la faire taire, une certaine organisation a lâché un assassin à ses trousses. La présence en ville d'un Tueur en série et d'un Policier confus aggrave la situation de plus belle. Les Protagonistes seront-ils capables de protéger la jeune fille ?\n</p>\n<h4>Conseils pour le Mastermind</h4>\n<p>\nEssayez de tuer la Lycéenne. Les différents moyens à votre disposition pour la tuer sont listés dans les \"Conditions de victoire\" ci-dessous. Le simple fait de jouer une carte sur la Lycéenne fera se questionner les Protagonistes sur la nature de la carte jouée. Jouez donc une carte dessus chaque jour. Durant la première boucle, les Protagonistes n'ont aucune idée de ce qu'il se passe. Il est conseillé de simplement déplacer la lycéenne vers le Tueur en série. À partir de la deuxième boucle, les Protagonistes seront conscients de la présence du Tueur en série. Vous pouvez placer des jetons Paranoïa sur la Lycéenne et l'Employé de bureau pour la faire mourir lors d'un incident, ou placer des jetons Intrigue sur elle pour la afin que l'assassin (l'Employé de bureau) la tue. Étant donné qu'il s'agit du premier script, vous trouverez dans les pages suivantes un guide détaillé pour la première fois que vous tentez ce script.\n</p>\n<h4>Si les protagonistes ne savent pas quoi faire</h4>\n<p>\nSi les Protagonistes ne semblent avoir aucune idée sur la marche à suivre, vous pouvez leur dire ceci : \"Gagner la première boucle temporelle est quasiment impossible. Il s'agit de récolter des informations. Placez des jetons Faveur sur certains personnages pour en utiliser les capacités.\"\n</p>",
        "mastermind_victory_conditions": "<ol>\n    <li>\n        <strong>Tuer le Personnage clé (Lycéenne)</strong><br/>\n        La capacité du Tueur / la capacité du Tueur en série / l'incident Meutre / l'incident Suicide\n    </li>\n    <li>\n        <strong>Tuer les Protagonistes</strong><br/>\n        La capacité du Tueur\n    </li>\n</ol>",
        "machination_primary": "meurtre_premedite",
        "machination_secondary_list": [
            "l_ombre_de_leventreur"
        ],
        "character_list": [
            {
                "characterCode": "lyceen",
                "roleCode": ""
            },
            {
                "characterCode": "lyceenne",
                "roleCode": "personnage_cle"
            },
            {
                "characterCode": "gardienne_du_sanctuaire",
                "roleCode": "tueur_en_serie"
            },
            {
                "characterCode": "policier",
                "roleCode": "conspirationniste"
            },
            {
                "characterCode": "employe_de_bureau",
                "roleCode": "tueur"
            },
            {
                "characterCode": "medecin",
                "roleCode": "cerveau"
            }
        ],
        "incident_list": [
            {
                "incidentCode": "meurtre",
                "jourNumero": "2",
                "coupable": "employe_de_bureau"
            },
            {
                "incidentCode": "suicide",
                "jourNumero": "3",
                "coupable": "lyceenne"
            }
        ]
    },
    {
        "tragedySet": "first_steps",
        "title": "Secret primordial",
        "origine_id": "2",
        "origine_tag": "BB",
        "origine": "Boîte de base",
        "informations": "<p>\n<strong>Créateur</strong> : BakaFire<br />\n<strong>Boucles</strong> : 4 / <strong>Difficulté</strong> : 1<br />\n<strong>Boucles</strong> : 3 / <strong>Difficulté</strong> : 3<br />\n<strong>Jours par boucle</strong> : 5\n</p>\n<h4>Particularités</h4>\n<p>\nCe script d'initiation est destiné aux joueurs qui ont déjà joué au premier script et qui veulent tenter un script différent sans ajouter de nouveaux personnages à mémoriser. Il est légèrement plus difficile que le premier. Il est conçu pour être joué en 4 boucles temporelles, mais vous pouvez le tenter en 3 afin de le rendre plus difficile pour les protagonistes.\n</p>\n<h4>Histoire</h4>\n<p>\nUn culte mystérieux a commencé à laver le cerveau des Lycéens afin de prendre le contrôle du Lycée.Le seul personnage capable de libérer leurs esprits est le psychiatre. Mais le culte s'en est aperçu, et a prévu de libérer un poison mortel dans l'Hôpital afin d'y tuer tout le monde.\n</p>\n<p>\nSi le lavage de cerveau des Lycéens se poursuit sans contrainte, ou si le psychiatre est tué, le culte réussira à accomplir son plan machiavélique. Pensez-vous que les Protagonistes seront capable de protéger le Lycée et l'Hôpital ?\n</p>\n<h4>Conseils pour le Mastermind</h4>\n<p>\nLe très puissant Incident à l'Hôpital se trouve dans ce script. La clé de votre succès réside dans son usage judicieux. Au jour 1, augmentez la Paranoïa de la Gardienne du Sanctuaire. Vous pourrez dès lors utiliser sa capacié de Conspirationniste pour lui ajouter encore de la paranoïa et déclencher l'incident Malaise grandissant. Ceci vous permettra de placer sur l'Hôpital les jetons Paranoïa nécessaire à l'Incident à l'Hôpital. Au troisième jour, utilisez-le pour tuer, au choix, le Médecin ou les Protagonistes. Vous devez utiliser l'Incident à l'Hôpital comme une menace. Si l'Hôpital possède deux jetons Intrigue, l'incident survient et les Protagonistes sont tués. Vous voudrez utiliser Intrigue+2 très vite pour déclencher l'Incident à l'Hôpital afin que, les jours suivants, dès que vous jouerez une carte sur l'Hôpital, ils la contrent avec une carte Pas d'Intrigue. Bluffez-les de façon à placer des jetons Intrigue sur le Lycée ou le Médecin ; vous gagnerez ainsi grâce à la règle de la machination principale (2 Jetons intrigue sur le Lycée) ou la mort du Médecin via l'incident Meurtre distant du jour 5.\n</p>",
        "mastermind_victory_conditions": "<ol>\n    <li>\n        <strong>Placer 2 Intrigue ou plus sur le Lycée</strong><br/>\n        Machination principale : Un lieu à protéger\n    </li>\n    <li>\n        <strong>Tuer le Personnage clé</strong><br/>\n        L'Incident à l'Hôpital/ l'incident Meurte distant\n    </li>\n    <li>\n        <strong>Tuer les Protagonistes</strong><br/>\n        L'Incident à l'Hôpital\n    </li>\n</ol>",
        "machination_primary": "un_lieu_a_proteger",
        "machination_secondary_list": [
            "rumeur_troublante"
        ],
        "character_list": [
            {
                "characterCode": "lyceen",
                "roleCode": "cultiste"
            },
            {
                "characterCode": "lyceenne",
                "roleCode": ""
            },
            {
                "characterCode": "gardienne_du_sanctuaire",
                "roleCode": "conspirationniste"
            },
            {
                "characterCode": "employe_de_bureau",
                "roleCode": ""
            },
            {
                "characterCode": "medecin",
                "roleCode": "personnage_cle"
            },
            {
                "characterCode": "patient",
                "roleCode": ""
            }
        ],
        "incident_list": [
            {
                "incidentCode": "malaise_grandissant",
                "jourNumero": "1",
                "coupable": "gardienne_du_sanctuaire"
            },
            {
                "incidentCode": "incident_a_l_hopital",
                "jourNumero": "2",
                "coupable": "lyceen"
            },
            {
                "incidentCode": "meurtre_distant",
                "jourNumero": "3",
                "coupable": "patient"
            }
        ]
    },
    {
        "tragedySet": "base_tragedy",
        "title": "Crêpage de chignons",
        "origine_id": "3",
        "origine_tag": "BB",
        "origine": "Boîte de base",
        "informations": "<p>\n<strong>Créateur</strong> : Gayusu<br />\n<strong>Boucles</strong> : 4 / <strong>Difficulté</strong> : 2<br />\n<strong>Boucles</strong> : 3 / <strong>Difficulté</strong> : 4<br />\n<strong>Jours par boucle</strong> : 6\n</p>\n<h4>Particularités</h4>\n<p>\nCe script est destiné à être le premier joué avec le recueil de Tragédies de base, tout de suite après les Premiers pas. Sa structure est très simple afin que même les joueurs inexpérimentés puissent l'apprécier.\n</p>\n<p>\nCependant, malgré sa simplicité, il nécessite un minimum de déduction. Des morts surviendront, l'identité du Personnage clé est toujours ignorée, il fonctionnera donc assez bien comme premier script.\n</p>\n<h4>Histoire</h4>\n<p>\nIl vient un moment dans la vie de chaque femme où il faut se battre. Trois jeunes filles se trouvent aujoud'hui à la croisée des chemns. Une chose mystérieuse est apparue et force les jeunes filles à faire un choix. L'une d'elle en a assez d'être l'élève modèle. Une autre observe, dépitée, le visage épuisé de la meilleure amie en rentrant du Lycée. Une chose est sure, pourtant : sans l'aide des Protagonistes, tout ceci terminera dans la douleur et le sang\n</p>\n<h4>Conseils pour le Mastermind</h4>\n<p>\nAucun rôle ne dispose de capacité de Mastermind dans ce script, il est donc assez simple à jouer. Essayez par contre de garder cette information secrête le plus longtemps possible. Vous devez faire attention au Tueur en série à la fin de chaque journée. Notez également que la mort d'un personnage peut affecter d'autres personnage quant à leur rôle.\n</p>\n<p>\nTant que le Tueur en série n'est pas découvert, placez une Intrigue+1 et deux Paranoïa +1 à chaque tour. À un moment durant la boucle, utilisez Intrigue +2 ; de préférence un jour d'incident. Une fois le Tueur en série démasqué, utilisez les déplacements sur l'amoureux et le Personnage clé pour les faire tuer. Durant la première boucle, vous devriez envisager la victoire grâce aux jetons Intrigue sur les filles. Placez de l'Intrigue sur les trois ; idéalement l'incident Mal odieux sera déclenché et les Protagonistes n'auront toujours pas identifié la machination principale. Si la première boucle ne se passe pas très bien, tuez l'Amoureux avec le Tueur en série durant la seconde boucle et gagnez grâce à la capacité de l'Être aimé. La troisième et quatrième boucle consiste principalement en une bataille sans relâche. Essayez autant que possible de garder l'Ami inconnu afin de garder un avantage lors de la Déduction finale.\n</p>",
        "mastermind_victory_conditions": "<ol>\n    <li>\n        <strong>Placer 2 Intrigue ou plus sur la Gardienne du Sanctuaire</strong><br/>\n        Machination principale : Choisis-moi !\n    </li>\n    <li>\n        <strong>Tuer le Personnage clé (Gardienne du Sanctuaire)</strong><br/>\n        La capacité du Tueur en série\n    </li>\n    <li>\n        <strong>Tuer l'Ami</strong><br/>\n        La capacité du Tueur en série/l'incident Suicide\n    </li>\n    <li>\n        <strong>Tuer les Protagonistes</strong><br/>\n        La capacité de l'Être aimé\n    </li>\n</ol>",
        "machination_primary": "choisis_moi",
        "machination_secondary_list": [
            "histoire_d_amour",
            "obsede_cache"
        ],
        "character_list": [
            {
                "characterCode": "lyceen",
                "roleCode": ""
            },
            {
                "characterCode": "lyceenne",
                "roleCode": "ami"
            },
            {
                "characterCode": "deleguee_de_classe",
                "roleCode": "etre_aime"
            },
            {
                "characterCode": "gardienne_du_sanctuaire",
                "roleCode": "personnage_cle"
            },
            {
                "characterCode": "policier",
                "roleCode": ""
            },
            {
                "characterCode": "employe_de_bureau",
                "roleCode": "amoureux"
            },
            {
                "characterCode": "informatrice",
                "roleCode": "tueur_en_serie"
            },
            {
                "characterCode": "patient",
                "roleCode": ""
            },
            {
                "characterCode": "infirmiere",
                "roleCode": ""
            }
        ],
        "incident_list": [
            {
                "incidentCode": "mal_odieux",
                "jourNumero": "3",
                "coupable": "employe_de_bureau"
            },
            {
                "incidentCode": "malaise_grandissant",
                "jourNumero": "4",
                "coupable": "deleguee_de_classe"
            },
            {
                "incidentCode": "suicide",
                "jourNumero": "6",
                "coupable": "lyceenne"
            }
        ]
    },
    {
        "tragedySet": "base_tragedy",
        "title": "Le moindre mal",
        "origine_id": "4",
        "origine_tag": "BB",
        "origine": "Boîte de base",
        "informations": "<p>\n<strong>Créateur</strong> : GEnd<br />\n<strong>Boucles</strong> : 4 / <strong>Difficulté</strong> : 3<br />\n<strong>Boucles</strong> : 3 / <strong>Difficulté</strong> : 4<br />\n<strong>Jours par boucle</strong> : 7\n</p>\n<h4>Particularités</h4>\n<p>\nLe Moindre mal est un script destiné aux joueurs qui ne sont pas encore familiers avec Tragedy. Il est adapté à une 1ère partie avec le recueil de Tragédie de base. Le Tueur en série et un personnage très important dans ce script, et le Mastermind devra l'utiliser au maximum.\n</p>\n<h4>Histoire</h4>\n<p>\nUn culte démoniaque organise en secret un terrible méfait et leur plan est sur le point de porter ses fruits. Le leader du culte, une femme mondaine aussi aisée qu'attirante, n'a plus besoin de se cacher. Mais elle avait omis un détail. Un homme banal, un bon-à-rien solitaire, s'avère avoir le meurtre comme passe-temps. Tapi dans l'ombre, il observe. Son couteau aiguisé pourrait parfaitement l'atteindre, elle aussi.\n</p>\n<h4>Conseils pour le Mastermind</h4>\n<p>\nD'habitude, vous voulez garder le Cerveau caché, mais pas dans ce script. Vous pouvez dévoiler sa capacité dès le jour 1. Durant la 1ère boucle, placez 2 jetons Intrigue sur le Lycée et le Centre-ville. Ainsi, l'Agent dormant peut placer de la Paranoïa sur lui-même et se suicider. Le premier jour devrait être Intrigue +1 et Intrigue +2 sur le Lycée et sur le Centre-ville et Paranoïa +1 sur la Riche héritière. À partir de la deuxième boucle, vous devrez faire plus attention. En fonction du placement des cartes, l'Agent dormat pourrait être tué par le Tueur en série, et sans Agent dormant, ce script est très délicat. Afin d'éviter cette situation, vous devriez jouer, durant le premier jour, 2 déplacements et Paranoïa +1 sur le Journaliste, l'Employé de bureau et la Riche héritière. Avec un peu de chance, les Protagonistes se focaliseront sur le blocage des Intrigues sur le Lycée et le Centre-ville. Tandis qu'ils s'affairent à ça, profitez en pour placer des Intrigues sur le Sanctuaire. Vous devez absolument garder l'Ami et le Cultistes dans l'ombre jusqu'à la Déduction finale. Vous devriez vraiment éviter d'utiliser la capacité du Cultiste, sauf en cas de force majeure.\n</p>",
        "mastermind_victory_conditions": "<ol>\n    <li>\n        <strong>Placer 2 Intrigue ou plus sur la Gardienne du Sanctuaire</strong><br/>\n        Machination principale : L'objet scellé\n    </li>\n    <li>\n        <strong>Tuer l'Ami (Gardienne du Sanctuaire)</strong><br/>\n        La capacité du Tueur en série\n    </li>\n    <li>\n        <strong>Tuer l'Agent dormant activé</strong><br/>\n        La capacité du Tueur en série/l'incident Suicide\n    </li>\n</ol>",
        "machination_primary": "l_objet_scelle",
        "machination_secondary_list": [
            "obsede_cache",
            "element_inconnu_x"
        ],
        "character_list": [
            {
                "characterCode": "lyceen",
                "roleCode": ""
            },
            {
                "characterCode": "lyceenne",
                "roleCode": ""
            },
            {
                "characterCode": "riche_heritiere",
                "roleCode": "cerveau"
            },
            {
                "characterCode": "gardienne_du_sanctuaire",
                "roleCode": "ami"
            },
            {
                "characterCode": "employe_de_bureau",
                "roleCode": "tueur_en_serie"
            },
            {
                "characterCode": "informatrice",
                "roleCode": ""
            },
            {
                "characterCode": "journaliste",
                "roleCode": "agent_dormant"
            },
            {
                "characterCode": "patient",
                "roleCode": ""
            },
            {
                "characterCode": "infirmiere",
                "roleCode": "cultiste"
            }
        ],
        "incident_list": [
            {
                "incidentCode": "malaise_grandissant",
                "jourNumero": "2",
                "coupable": "riche_heritiere"
            },
            {
                "incidentCode": "personne_disparue",
                "jourNumero": "4",
                "coupable": "infirmiere"
            },
            {
                "incidentCode": "personne_disparue",
                "jourNumero": "5",
                "coupable": "lyceen"
            },
            {
                "incidentCode": "suicide",
                "jourNumero": "7",
                "coupable": "journaliste"
            }
        ]
    },
    {
        "tragedySet": "base_tragedy",
        "title": "Un secret bien gardé",
        "origine_id": "5",
        "origine_tag": "BB",
        "origine": "Boîte de base",
        "informations": "<p>\n<strong>Créateur</strong> : BakaFire<br />\n<strong>Boucles</strong> : 4 / <strong>Difficulté</strong> : 3<br />\n<strong>Boucles</strong> : 3 / <strong>Difficulté</strong> : 5<br />\n<strong>Jours par boucle</strong> : 7\n</p>\n<h4>Particularités</h4>\n<p>\nCe script convient aux joueurs qui ne sont pas encore familiers avec Tragedy Looper. Il est adapté à une 1ère partie avec le recueil de Tragédie de base. L'objectif de ce script de souligner l'importance des capacités Faveur et de leur refus.  \n</p>\n<h4>Histoire</h4>\n<p>\nC'est une sorcière. Il est primordial que ce secret demeure bien gardé, qu'elle soit la cause de la grande destruction, ou juste un pion que d'autres manipulent. Les affreux événements qui surviennent en ville, couplés à la vicieuse spirale du destin camouffle la présence d'une bombe temporelle. Et le secret fût gardé, même pour la Sorcière elle même...\n</p>\n<h4>Conseils pour le Mastermind</h4>\n<p>\nDans toute les boucles, vous devriez opter pour le Suicide lors du deuxième jour. Cela devrait être possible avec l'aide du Conspirationniste et des Fils du destin. De plus, jusqu'à son Suicide, veuillez à ce que sa capacité Faveur ne puisse être utilisée ; jouez sur elle une carte Pas de faveurs au besoin. De cette manière, elle ne sera pas révélée à cause de son Refus de Faveur obligatoire, et la localisation de la bombe temporelle ne sera pas révélée sauf nécessité absolue. La Conspirationniste devrait utiliser sa capacité dès le premier jour, afin de provoquer autant d'incidents que possible. Pas uniquement le Suicide, mais aussi la Personne disparue et le Mal audieux, cela aidera à cacher la bombe. Cela ne devrait pas être si difficile ; les incidents sont prévus pour se déclencher assez facilement. Cependant, aucun d'entre eux ne pourra mettre d'Intrigue sur le Lycée, vous devrez y placer de l'intrigue via vos cartes. Utilisez la menace d'un Incident à l'hôpital pour bluffer. Les Protagonistes tenteront très probablement d'activer autant de Faveur que possible. Même s'ils ne parviennent pas à dénicher la Sorcière, ils pourront savoir qui ne l'est pas juste par l'usage des capacités faveur des personnages. Pour la déduction finale, vous devriez cacher l'Extra-terrestre (Ami). Le script est conçu pour qu'elle ne puisse pas mourir, la cacher ne devrait donc pas être bien difficile. \n</p>",
        "mastermind_victory_conditions": "<ol>\n    <li>\n        <strong>Placer 2 Intrigue ou plus sur le Lycée</strong><br/>\n        Machination principale : Bombe temporelle\n    </li>\n    <li>\n        <strong>Tuer l'Ami (Employé de bureau)</strong><br/>\n        L'incident à l'Hôpital\n    </li>\n    <li>\n        <strong>Tuer les Protagonistes</strong><br/>\n        L'incident à l'Hôpital\n    </li>\n</ol>",
        "machination_primary": "bombe_temporelle",
        "machination_secondary_list": [
            "fils_du_destin",
            "cercle_d_amis"
        ],
        "character_list": [
            {
                "characterCode": "riche_heritiere",
                "roleCode": "sorciere"
            },
            {
                "characterCode": "deleguee_de_classe",
                "roleCode": ""
            },
            {
                "characterCode": "gardienne_du_sanctuaire",
                "roleCode": ""
            },
            {
                "characterCode": "extraterrestre",
                "roleCode": "ami"
            },
            {
                "characterCode": "employe_de_bureau",
                "roleCode": "ami"
            },
            {
                "characterCode": "informatrice",
                "roleCode": "conspirationniste"
            },
            {
                "characterCode": "pop_star",
                "roleCode": ""
            },
            {
                "characterCode": "journaliste",
                "roleCode": ""
            },
            {
                "characterCode": "patient",
                "roleCode": ""
            }
        ],
        "incident_list": [
            {
                "incidentCode": "suicide",
                "jourNumero": "2",
                "coupable": "riche_heritiere"
            },
            {
                "incidentCode": "personne_disparue",
                "jourNumero": "3",
                "coupable": "employe_de_bureau"
            },
            {
                "incidentCode": "incident_a_l_hopital",
                "jourNumero": "4",
                "coupable": "journaliste"
            },
            {
                "incidentCode": "contagion",
                "jourNumero": "6",
                "coupable": "gardienne_du_sanctuaire"
            },
            {
                "incidentCode": "mal_odieux",
                "jourNumero": "7",
                "coupable": "pop_star"
            }
        ]
    },
    {
        "tragedySet": "base_tragedy",
        "title": "L'avenir des dieux",
        "origine_id": "6",
        "origine_tag": "BB",
        "origine": "Boîte de base",
        "informations": "<p>\n<strong>Créateur</strong> : Nightly Moonfire group<br />\n<strong>Boucles</strong> : 4 / <strong>Difficulté</strong> : 4<br />\n<strong>Jours par boucle</strong> : 7\n</p>\n<h4>Particularités</h4>\n<strong style=\"color:red;\">Ce script contient le personnage Être divin qui arrive à partir d'une certaine boucle. Les Protagonistes n'apprennent son existence qu'à son arrivée sur le plateau, pas avant.</strong><p>\nCe script est prévu pour des joueurs qui commence à être familier avec Tragedy Looper. Il n'est pas conseillé pour une 1ère partie avec le recueil de Tragédie de base. Jouez au moins un autre script avant de tenter celui-ci. Ce script inclut un personnage très important : l'Être divin. Régalez vous du chaos qui surviendra suite à la spirale temporel. Il est fortement recommandé de ne pas autoriser les discussions durant cette partie, sauf durant la spirale.\n</p>\n<h4>Histoire</h4>\n<p>\nÀ moins qu'un acte ne soit accompli, le futur est corrompu. Un garçon provenant d'un futur lointain a remonté le temps pour éviter ce drame. La croisée des chemins se trouve ici, cette semaine, dans cette ville.\n</p>\n<p>\nLa ville entière est en effervescence, car une Pop star très célèbre et de passage. C'est à partir d'elle, et de son fan le Policier que se ramifient les possibilités d'un futur corrompu. Comment est-elle parvenue à être si populaire ? Elle est bénie par les dieux. Ce futur corrompu et conçu pour elle. C'est le futur des dieux. \n</p>\n<h4>Conseils pour le Mastermind</h4>\n<p>\nVous avez quelques options pour remporter la victoire durant la 1ère moitié de chaque boucle. Cela signifie aussi que les Protagonistes auront très peu d'informations. Planifier ainsi une victoire ne devrait pas être bien difficile. Vous devriez anticiper plusieurs boucles. Ainsi, tentez d'atteindre la deuxième boucle sans révéler qui est l'Ami où la Machination principal. L'Être divin entre en jeu durant la troisième boucle. Placez une intrigue dessus, déplacer le Policier et optez pour une fin rapide grâce à la capacité de l'Être aimé. Ce script est conçu afin que l'Histoire d'amour ne soit pas révélée avant la deuxième boucle, cela devrait donc surgir comme une attaque-surprise. Si vous pensez y arriver, tentez de cacher l'Amoureux jusqu'à la fin, mais attention, ça risque d'être corsé ! La quatrième boucle consiste en une guerre en tous sens. La déduction finale sera éprouvante pour vous, vu qu'il n'y a pas beaucoup de rôles faciles à camoufler. Improvisez au mieux. Vous pouvez par exemple cacher le Voyageur temporel où l'Ami en vous concentrant sur le Tueur en série, ou garder le Cultistes caché en vous concentrant sur les capacités Faveur. Il est plus judicieux de garder les rôles cachés plutôt que de prendre le risque de vous dévoiler, en quête d'une victoire rapide.\n</p>",
        "mastermind_victory_conditions": "<ol>\n    <li>\n        <strong>Déclencher l'Effet papillon</strong><br/>\n        Machination principale : Futur boulversé\n    </li>\n    <li>\n        <strong>Finir le dernier jour de la boucle avec 2 Faveur ou moins sur le Lycéen (Voyageur temporel)</strong><br/>\n        Capacité du Voyageur temporel\n    </li>\n    <li>\n        <strong>Tuer l'Ami (Infirmière)</strong><br/>\n        La capacité du Tueur en série\n    </li>\n    <li>\n        <strong>Tuer les Protagonistes</strong><br/>\n        La capacité de l'Être aimé\n    </li>\n</ol>",
        "machination_primary": "futur_boulverse",
        "machination_secondary_list": [
            "obsede_cache",
            "histoire_d_amour"
        ],
        "character_list": [
            {
                "characterCode": "lyceen",
                "roleCode": "voyageur_temporel"
            },
            {
                "characterCode": "riche_heritiere",
                "roleCode": ""
            },
            {
                "characterCode": "gardienne_du_sanctuaire",
                "roleCode": "cultiste"
            },
            {
                "characterCode": "etre_divin",
                "roleCode": "etre_aime",
                "additionnal_field": "Entre en jeu à la boucle 3"
            },
            {
                "characterCode": "policier",
                "roleCode": ""
            },
            {
                "characterCode": "employe_de_bureau",
                "roleCode": "tueur_en_serie"
            },
            {
                "characterCode": "pop_star",
                "roleCode": "amoureux"
            },
            {
                "characterCode": "patient",
                "roleCode": ""
            },
            {
                "characterCode": "infirmiere",
                "roleCode": "ami"
            }
        ],
        "incident_list": [
            {
                "incidentCode": "suicide",
                "jourNumero": "2",
                "coupable": "pop_star"
            },
            {
                "incidentCode": "malaise_grandissant",
                "jourNumero": "4",
                "coupable": "gardienne_du_sanctuaire"
            },
            {
                "incidentCode": "effet_papillon",
                "jourNumero": "5",
                "coupable": "policier"
            },
            {
                "incidentCode": "mal_odieux",
                "jourNumero": "7",
                "coupable": "patient"
            }
        ]
    },
    {
        "tragedySet": "base_tragedy",
        "title": "Yin et Yang",
        "origine_id": "7",
        "origine_tag": "BB",
        "origine": "Boîte de base",
        "informations": "<p>\n<strong>Créateur</strong> : M. Hydromel<br />\n<strong>Boucles</strong> : 4 / <strong>Difficulté</strong> : 4<br />\n<strong>Boucles</strong> : 6 / <strong>Difficulté</strong> : 5<br />\n<strong>Jours par boucle</strong> : 7\n</p>\n<h4>Particularités</h4>\n<p>\nCe script est conçu pour les joueurs habitués à Tragedy Looper. Il n'est pas recommandé en tant que premier script du recueil de Tragédie de base. Vous devriez au moins avoir effectué une tragédie de base avant de tenter celle-ci. Ce script inclut un personnage déroutant, le Garçon mystérieux. À lui seul, il peut compliquer le script le plus simple.\n</p>\n<h4>Histoire</h4>\n<p>\nDeux filles partagent le même destin, en miroir l'une de l'autre. L'une est lumière, l'autre ombre, dissimulée. Si l'une d'entre elles meurt, le monde s'effondrera. Seule la fille de lumière peut conclure le pacte qui peut sauver l'univers. La fille de l'ombre ne sais que se défendre timidement, se taire, ou être protégée. Parfois, le monde est cruellement simple. Ou bien aurai-t-il du l'être...\n</p>\n<p>\nCette histoire apparemment simple est rapidement embrouillée par l'arrivée d'un facteur inattendue : l'autre coté du miroir, l'adorateur des ténèbres, le fanatique sans-âme. Avec son aide, le coté obscur gagne de l'influence. Lumière et ombre se confondent mutuellement, la frontière devient flou et ces puissances fusionnent pour devenir un. Seuls les protagonistes sont à même de les distinguer.\n</p>\n<h4>Conseils pour le Mastermind</h4>\n<p>\nGardez toujours en tête que, même si tous les autres rôles sont révélés, les Protagonistes ne doivent pas découvrir quelle fille est le Personnage clé. À chaque boucle, vos actions du jour un sont déjà réglées. 1ère boucle : Intrique +2 sur le Lycée, Intrigue +1 sur le Sanctuaire, et une carte bluff sur l'Hôpital. Votre but n'est pas de déclencher l'Incident à l'hôpital, continuez plutôt à bluffer tout le long du jeu. Durant la deuxième boucle, Intrigue +2 sur le Lycée, Intrigue +1 sur le Centre-ville et bluff sur le Sanctuaire. Par contre, la capacité du Garçon mystérieux révèlera que les intrigues sur le sanctuaire sont inoffensives. À partir de ce point, bluffer sur le Sanctuaire devient inutile ; ce n'est qu'une perte d'actions. Durant la troisième boucle, jouez sur le Lycée, le Centre-ville et sur l'une des filles. Deux cartes jouées doivent être des cartes intrigue. Utilisez au besoin la capacité du Cultiste dans toutes les situations ci-dessus. À partir du jour 2, concentrez-vous sur les lieux et les 2 filles. Votre objectif pour la 1ère boucle doit être d'avoir 2 intriques sur chaque fille et sur le Sanctuaire. À partir de la deuxième boucle, ayez 2 Intrigue sur chaque fille et sur le Centre-ville. Une fois que l'Agent dormant possède la capacité du Personnage clé, tuez n'importe lequel d'entre eux ne révèlera pas le vrai. De plus, il est toujours bon d'avoir des Intrigues sur le Lycée afin de cacher le Conspirationniste. \n</p>",
        "mastermind_victory_conditions": "<ol>\n    <li>\n        <strong>Placer 2 Intrigues ou plus sur la Lycéenne</strong><br/>\n        Machination principale : Choisis-moi !\n    </li>\n    <li>\n        <strong>Tuer le Personnage clé (Lycéenne ou l'Agent dormant activé)</strong><br/>\n        La capacité du Tueur en série/l'Incident à l'Hôpital/l'incident Meurtre\n    </li>\n    <li>\n        <strong>Tuer les Protagonistes</strong><br/>\n        L'Incident à l'Hôpital\n    </li>\n</ol>",
        "machination_primary": "choisis_moi",
        "machination_secondary_list": [
            "element_inconnu_x",
            "paranoia_mortelle"
        ],
        "character_list": [
            {
                "characterCode": "lyceen",
                "roleCode": ""
            },
            {
                "characterCode": "lyceenne",
                "roleCode": "personnage_cle"
            },
            {
                "characterCode": "riche_heritiere",
                "roleCode": "agent_dormant"
            },
            {
                "characterCode": "garcon_mysterieux",
                "roleCode": "cultiste"
            },
            {
                "characterCode": "employe_de_bureau",
                "roleCode": ""
            },
            {
                "characterCode": "informatrice",
                "roleCode": "conspirationniste"
            },
            {
                "characterCode": "journaliste",
                "roleCode": ""
            },
            {
                "characterCode": "patient",
                "roleCode": ""
            },
            {
                "characterCode": "infirmiere",
                "roleCode": ""
            }
        ],
        "incident_list": [
            {
                "incidentCode": "personne_disparue",
                "jourNumero": "3",
                "coupable": "riche_heritiere"
            },
            {
                "incidentCode": "malaise_grandissant",
                "jourNumero": "4",
                "coupable": "journaliste"
            },
            {
                "incidentCode": "incident_a_l_hopital",
                "jourNumero": "5",
                "coupable": "garcon_mysterieux"
            },
            {
                "incidentCode": "meurtre",
                "jourNumero": "7",
                "coupable": "lyceen"
            }
        ]
    },
    {
        "tragedySet": "base_tragedy",
        "title": "Immunisés",
        "origine_id": "8",
        "origine_tag": "BB",
        "origine": "Boîte de base",
        "informations": "<p>\n<strong>Créateur</strong> : Satoshi Sawamura<br />\n<strong>Boucles</strong> : 5 / <strong>Difficulté</strong> : 5<br />\n<strong>Boucles</strong> : 4 / <strong>Difficulté</strong> : 6<br />\n<strong>Jours par boucle</strong> : 4\n</p>\n<h4>Particularités</h4>\n<p>\nCe script est conçu pour les joueurs habitués à Tragedy Looper. Vous devriez avoir joué à au moins 2 Tragédie de base avant ce script. Ce script a beaucoup de boucles temporelles, mais chaque boucle est très courte. Il y'a beaucoup de choses à suivre et il est construit comme un puzzle. Cependant, ce n'est pas un puzzle de logique, et il revient aux Protagonistes d'en trouver la solution hors de l'ordinaire.\n</p>\n<h4>Histoire</h4>\n<p>\nUne épidémie n'est ni plus ni moins qu'un futur inévitable. Tout cela a commencé avec le battement d'ailes d'un papillon, dont le léger souffle est devenu vent, puis bourrasques, avant de devenir ce maelstrom dont personne n'échappera. Les Protagonistes ne peuvent y échapper. Le futur ne peut être changé.\n</p>\n<p>\nEt si ce futur ne peut être changé, alors il doit être accepté. Si vous pouvez éviter le virus, alors peut-être trouverez-vous le moyen de l'arrêter. Vous devrez donc trouver ceux qui sont immunisés.\n</p>\n<h4>Conseils pour le Mastermind</h4>\n<p>\nDans ce script, sauf échec critique, vous gagnerez chacune des boucles temporelles. En d'autres termes, la seule chance de gagner, pour les Protagonistes, réside dans la Déduction Finale. Leur destin dépend de la rapidité avec laquelle il le comprendront, et du nombre d'informations qu'ils pourront récolter pour étoffer leur déduction.\n</p>\n<p>\nPlacez-le Sbire au Lycée au début de chaque boucle. Pour le premier jour de la 1ère boucle, jouez Paranoïa +1 sur la Riche héritière et le Sbire, et une carte Pas de faveur sur la Lycéenne. Utilisez ensuite la capacité Effet papillon pour placer un jeton Faveur sur la Riche héritière (si vous pensez que vous aurez du mal à déclencher le Mal odieux, de la Paranoïa sur le Sbire fera l'affaire). Si vous faites ceci, les Fils du destin feront se déclencher l'Effet papillon à chaque boucle temporelle. Les Protagonistes tenteront sans doute de découvrir la Machination principale en premier. Déclenchez le Mal Odieu pour gagner du temps.\n</p>\n<p>\nLa possibilité de gagner réside pour les Protagonistes dans le placement de jetons Paranoïa pour transformer les Individus en Tueur en série. En voyant les personnages qui ne changent pas et ceux qui ne meurent pas, ils pourront déduire les individus. Pour vous, utiliser les Fils du destin pour faciliter la gestion de la Paranoïa la clé.\n</p>",
        "mastermind_victory_conditions": "<ol>\n    <li>\n        <strong>Déclencher l'Effet papillon</strong><br/>\n        Machination principale : Futur boulversé\n    </li>\n    <li>\n        <strong>Finir le dernier jour de la boucle temporelle avec 2 Faveurs ou moins sur le Sbire</strong><br/>\n        Capacité du Voyageru temporel\n    </li>\n</ol>",
        "machination_primary": "futur_boulverse",
        "machination_secondary_list": [
            "fils_du_destin",
            "paranoia_mortelle"
        ],
        "character_list": [
            {
                "characterCode": "lyceenne",
                "roleCode": ""
            },
            {
                "characterCode": "riche_heritiere",
                "roleCode": "conspirationniste"
            },
            {
                "characterCode": "deleguee_de_classe",
                "roleCode": ""
            },
            {
                "characterCode": "gardienne_du_sanctuaire",
                "roleCode": "cultiste"
            },
            {
                "characterCode": "policier",
                "roleCode": ""
            },
            {
                "characterCode": "employe_de_bureau",
                "roleCode": ""
            },
            {
                "characterCode": "informatrice",
                "roleCode": ""
            },
            {
                "characterCode": "medecin",
                "roleCode": ""
            },
            {
                "characterCode": "patient",
                "roleCode": ""
            },
            {
                "characterCode": "sbire",
                "roleCode": "voyageur_temporel"
            }
        ],
        "incident_list": [
            {
                "incidentCode": "effet_papillon",
                "jourNumero": "1",
                "coupable": "riche_heritiere"
            },
            {
                "incidentCode": "mal_odieux",
                "jourNumero": "2",
                "coupable": "sbire"
            },
            {
                "incidentCode": "contagion",
                "jourNumero": "3",
                "coupable": "medecin"
            },
            {
                "incidentCode": "personne_disparue",
                "jourNumero": "4",
                "coupable": "policier"
            }
        ]
    },
    {
        "tragedySet": "base_tragedy",
        "title": "Prologue",
        "origine_id": "9",
        "origine_tag": "BB",
        "origine": "Boîte de base",
        "informations": "<p>\n<strong>Créateur</strong> : BakaFire<br />\n<strong>Boucles</strong> : 5 / <strong>Difficulté</strong> : 5<br />\n<strong>Boucles</strong> : 4 / <strong>Difficulté</strong> : 7<br />\n<strong>Jours par boucle</strong> : 7\n</p>\n<h4>Particularités</h4>\n<p>\nPrologue est un script qui marque le début de la fin. Il est destiné aux joueurs qui ont souvent joué à Tragedy Looper, peu importe le recueil.\n</p>\n<p>\nCe script considère que les joueurs sont familiers avec les différents éléments, mènent des récoltes d'information efficace, savent planifier durant une spirale temporelle et possèdent de bonnes compétences déductives\n</p>\n<p>\nVous pouvez en baisser la difficulté en jouant 5 boucle temporelle, mais nous vous recommandons d'essayer en 4. Veillez à ce que les joueurs ne parlent pas autour de la table.\n</p>\n<h4>Histoire</h4>\n<p>\nAprès avoir surmonté de nombreux scénarios, les Protagonistes sont arrivés sur un script particulièrement délicat. Tous les scénarios précédents avaient pour objectif de vous préparer graduellement à faire face à celui-ci. Bienvenue au début de la fin. Parce qu'au bout du compte, c'est de ça qu'il s'agissait, depuis le début...\n</p>\n<h4>Conseils pour le Mastermind</h4>\n<p>\nCette fois-ci, vous devrez vous débrouiller par vous-même. Mais rassurez-vous, voici tout de même quelques conseils. Ce script peut vous sembler simple, mais de nombreux détails combinés en font un amalgame particulièrement tordu. Le personnage clé est une fille. Les 2 autres filles ont des rôles qui impliquent les jetons Intrigues. Le médecin peut refuser les capacités Faveur. Le seul Individu est l'Employé de bureau. Nous avons l'Incident à l'Hôpital, et un Ami qui ne peut bouger. Le coupable de l'Incident du deuxième jour et le Conspirationniste commencent la partie dans le même lieu de départ. L'incident sucide et l'Incident à l'Hôpital sont inextricablement liées au travers de l'Amoureux de l'Être aimé. De plus, la présence de paranoïa sur la Lycéenne mène directement les Protagonistes à leur mort.\n</p>",
        "mastermind_victory_conditions": "<ol>\n    <li>\n        <strong>Tuer le Personnage clé (Gardienne du Sanctuaire)</strong><br/>\n        La capacité du Tueur/l'Incident à l'Hôpital/l'incident Meurtre\n    </li>\n    <li>\n        <strong>Tuer n'importe lequel des Amis (Informatrice ou Patient)</strong><br/>\n        L'Incident à l'Hôpital/l'incident Meurtre\n    </li>\n    <li>\n        <strong>Tuer les Protagonistes</strong><br/>\n        La capacité de l'Être aimé/la capacité du Tueur/l'Incident à l'Hôpital\n    </li>\n</ol>",
        "machination_primary": "meurtre_premedite",
        "machination_secondary_list": [
            "cercle_d_amis",
            "histoire_d_amour"
        ],
        "character_list": [
            {
                "characterCode": "lyceen",
                "roleCode": "amoureux"
            },
            {
                "characterCode": "lyceenne",
                "roleCode": "etre_aime"
            },
            {
                "characterCode": "riche_heritiere",
                "roleCode": "tueur"
            },
            {
                "characterCode": "gardienne_du_sanctuaire",
                "roleCode": "personnage_cle"
            },
            {
                "characterCode": "policier",
                "roleCode": "conspirationniste"
            },
            {
                "characterCode": "employe_de_bureau",
                "roleCode": ""
            },
            {
                "characterCode": "informatrice",
                "roleCode": "ami"
            },
            {
                "characterCode": "medecin",
                "roleCode": "cerveau"
            },
            {
                "characterCode": "patient",
                "roleCode": "ami"
            }
        ],
        "incident_list": [
            {
                "incidentCode": "malaise_grandissant",
                "jourNumero": "2",
                "coupable": "employe_de_bureau"
            },
            {
                "incidentCode": "suicide",
                "jourNumero": "4",
                "coupable": "lyceenne"
            },
            {
                "incidentCode": "incident_a_l_hopital",
                "jourNumero": "5",
                "coupable": "lyceen"
            },
            {
                "incidentCode": "meurtre",
                "jourNumero": "7",
                "coupable": "policier"
            }
        ]
    },
    {
        "tragedySet": "base_tragedy",
        "title": "Une tragédie sans fin",
        "origine_id": "10",
        "origine_tag": "BB",
        "origine": "Boîte de base",
        "informations": "<p>\n<strong>Créateur</strong> : BF + 3G<br />\n<strong>Boucles</strong> : 4 / <strong>Difficulté</strong> : 8<br />\n<strong>Jours par boucle</strong> : 6\n</p>\n<h4>Règle spéciale</h4>\n<p>\nEn début de partie, le Mastermind retire la carte \"Pas de faveur\" de sa main. Cette carte ne peut pas être utilisée durant la partie.\n</p>\n<h4>Particularités</h4>\n<p><strong style=\"color:red;\">Ce script contient le personnage Être divin qui arrive à partir d'une certaine boucle. Les Protagonistes n'apprennent son existence qu'à son arrivée sur le plateau, pas avant.</strong></p><p>\nCe script est le plus tordu de tous les scripts.\n</p>\n<h4>Histoire</h4>\n<p>\nIl existe une infinité de mondes possibles. Il existe une infinité de légendes à conter. D'innombrables scripts furent écrits, et nombre de Mastermind ricanèrent, crièrent ou geignirent. Pendant un temps infini, ceci fut répété, et les pensées de désespoir, de douleur et d'espérance de nombreux Protagonistes furent révélés. Ensuite tous ces mondes entrèrent en collision et ne firent plus qu'un. Le résultat fut désastreux.\n</p>\n<p>\nVous êtes au seuil d'une dimension en effervescence, grouillant d'une multitude de possibles. C'est une tragédie éternelle. Une tragédie sans fin. Amusez vous bien.\n</p>\n<p>Avec la boucle suivante...\n</p>\n<h4>Conseils pour le Mastermind</h4>\n<p>\nCe script n'est pas seulement un cauchemard pour les Protagonistes, c'est également un casse tête incontrôlable de 1ère classe pour le Mastermind. Votre victoire principale sera remportée à la fin des boucles, mais vous ne pouvez pas vous contenter de ça. Vous devez étendre les possibilités, et vous arranger pour que la Machination principale ne soit pas révélée. Les jetons Intrigues sur le Sanctuaire sont indispensables. Ensuite sur l'Hôpital, la Lycéenne, la Déléguée de classe, la Pop Star et l'Extraterrestre. Placer 2 Intrigues sur tous ceux que vous pouvez de cette liste. Utilisez le Cerveau et le Conspirationistes durant le premier jour de la 1ère boucle. Déclenchez l'Effet papillon afin de vous laisser la possibilité d'un Futur bouleversée. Durant la 1ère où la deuxième boucle, gagnez avec la capacité de l'Être aimé. Dévoiler tôt sa nature forcera les Protagonistes à vouloir s'en protéger, ce qui facilitera votre placement de jetons intrigue. Si les capacités du Garçon mystérieux et de l'Extraterrestre sont utilisées, les Protagonistes croiront que le Sanctuaire est un bluff. Utiliser la troisième boucle pour accumuler de l'Intrigue dessus. Au bout de la quatrième boucle, vous devriez pouvoir lire le désexpoir dans leur yeux. Étant donné que la carte \"Pas de faveur\" est retirée de la main du Mastermind, les Protagonistes en profiteront abondamment. S'ils s'en servent judicieusement, il existe pour eux une infime possibilité de l'emporter.\n</p>",
        "mastermind_victory_conditions": "<ol>\n    <li>\n        <strong>Avoir 2 Intrigues ou plus sur le Sanctuaire</strong><br/>\n        Machination principale : Bombe temporelle\n    </li>\n    <li>\n        <strong>Tuer les Protagonistes</strong><br/>\n        La capacité de l'Être aimé\n    </li>\n</ol>",
        "machination_primary": "bombe_temporelle",
        "machination_secondary_list": [
            "rumeur_troublante",
            "histoire_d_amour"
        ],
        "character_list": [
            {
                "characterCode": "lyceenne",
                "roleCode": ""
            },
            {
                "characterCode": "riche_heritiere",
                "roleCode": "etre_aime"
            },
            {
                "characterCode": "deleguee_de_classe",
                "roleCode": ""
            },
            {
                "characterCode": "garcon_mysterieux",
                "roleCode": "cerveau"
            },
            {
                "characterCode": "extraterrestre",
                "roleCode": ""
            },
            {
                "characterCode": "etre_divin",
                "roleCode": "sorciere",
                "additionnal_field": "Entre en jeu à la boucle 4"
            },
            {
                "characterCode": "employe_de_bureau",
                "roleCode": ""
            },
            {
                "characterCode": "pop_star",
                "roleCode": ""
            },
            {
                "characterCode": "boss",
                "roleCode": "conspirationniste",
                "additionnal_field": "Territoire : Lycée"
            },
            {
                "characterCode": "patient",
                "roleCode": "amoureux"
            },
            {
                "characterCode": "infirmiere",
                "roleCode": ""
            }
        ],
        "incident_list": [
            {
                "incidentCode": "effet_papillon",
                "jourNumero": "2",
                "coupable": "deleguee_de_classe"
            },
            {
                "incidentCode": "malaise_grandissant",
                "jourNumero": "3",
                "coupable": "extraterrestre"
            },
            {
                "incidentCode": "personne_disparue",
                "jourNumero": "4",
                "coupable": "employe_de_bureau"
            },
            {
                "incidentCode": "effet_papillon",
                "jourNumero": "5",
                "coupable": "infirmiere"
            },
            {
                "incidentCode": "personne_disparue",
                "jourNumero": "6",
                "coupable": "patient"
            }
        ]
    }
]);
