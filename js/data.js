
var mainData = {
    "game_step_list":{
        "boucle_debut":"Début de boucle",
        "journee_debut":"Début de la journée - Matin",
        "actions_mastermind":"Placement des cartes du Mastermind",
        "actions_protagonistes":"Placement des cartes des Protagonistes",
        "resolution_cartes":"Résolution des cartes",
        "capacites_mastermind":"Capacités du Mastermind",
        "faveurs":"Utilisation des Faveurs par le Leader",
        "incidents":"Déclenchement des incidents",
        "journee_fin":"Fin de journée - Nuit",
        "boucle_fin":"Fin de boucle",
        "creation_script":"Création de script",
        "passif":"Passif"
    },
    "character_tag_list":{
        "etudiant":"Étudiant(e)",
        "adulte":"Adulte",
        "masculin":"Masculin",
        "feminin":"Feminin",
        "creation":"Création",
        "animal":"Animal"
    },
    "character_list":{
        "lyceen":{
            "name":"Lycéen",
            "image_css_class":"card_lyceen",
            "board_game_box_origine":"Boîte de base",
            "paranoya_limit":2,
            "tags":[
                "etudiant",
                "masculin"
            ],
            "faveurs":[
                {
                    "cost":2,
                    "effect":"-1 Paranoïa à un(e) autre étudiant(e) dans ce lieu."
                }
            ],
            "additionnal_rule_list":[]
        },
        "lyceenne":{
            "name":"Lycéenne",
            "image_css_class":"card_lyceenne",
            "board_game_box_origine":"Boîte de base",
            "paranoya_limit":3,
            "tags":[
                "etudiant",
                "feminin"
            ],
            "faveurs":[
                {
                    "cost":2,
                    "effect":"-1 Paranoïa à un(e) autre étudiant(e) dans ce lieu."
                }
            ],
            "additionnal_rule_list":[]
        },
        "riche_heritiere":{
            "name":"Riche héritière",
            "image_css_class":"card_riche_heritiere",
            "board_game_box_origine":"Boîte de base",
            "paranoya_limit":1,
            "tags":[
                "etudiant",
                "feminin"
            ],
            "faveurs":[
                {
                    "cost":3,
                    "effect":"Si au lycée ou au centre-ville, +1 Faveur à un personnage dans ce lieu."
                }
            ],
            "additionnal_rule_list":[]
        },
        "deleguee_de_classe":{
            "name":"Déléguée de classe",
            "image_css_class":"card_deleguee_de_classe",
            "board_game_box_origine":"Boîte de base",
            "paranoya_limit":2,
            "tags":[
                "etudiant",
                "feminin"
            ],
            "faveurs":[
                {
                    "cost":2,
                    "effect":"Une fois par boucle, le leadeur reprend une carte \"une fois par boucle\"."
                }
            ],
            "additionnal_rule_list":[]
        },
        "garcon_mysterieux":{
            "name":"Garçon mystérieux",
            "image_css_class":"card_garcon_mysterieux",
            "board_game_box_origine":"Boîte de base",
            "paranoya_limit":3,
            "tags":[
                "etudiant",
                "masculin"
            ],
            "faveurs":[
                {
                    "cost":3,
                    "effect":"(Seulement à partir de la seconde boucle. Cette capacité est immunisée au Refus de Faveur.) Révélez le rôle de ce personnage."
                }
            ],
            "additionnal_rule_list":[]
        },
        "gardienne_du_sanctuaire":{
            "name":"Gardienne du sanctuaire",
            "image_css_class":"card_gardienne_du_sanctuaire",
            "board_game_box_origine":"Boîte de base",
            "paranoya_limit":2,
            "tags":[
                "etudiant",
                "feminin"
            ],
            "faveurs":[
                {
                    "cost":3,
                    "effect":"Si au Sanctuaire, -1 Intrigue sur le Sanctuaire."
                },
                {
                    "cost":5,
                    "effect":"Une fois par boucle, révélez le rôle d'un personnage dans ce lieu."
                }
            ],
            "additionnal_rule_list":[]
        },
        "extraterrestre":{
            "name":"Extraterrestre",
            "image_css_class":"card_extraterrestre",
            "board_game_box_origine":"Boîte de base",
            "paranoya_limit":2,
            "tags":[
                "feminin"
            ],
            "faveurs":[
                {
                    "cost":4,
                    "effect":"Une fois par boucle, un autre personnage dans ce lieu décède."
                },
                {
                    "cost":5,
                    "effect":"Une fois par boucle, ressuscitez un cadavre dans ce lieu."
                }
            ],
            "additionnal_rule_list":[]
        },
        "etre_divin":{
            "name":"Être divin",
            "image_css_class":"card_etre_divin",
            "board_game_box_origine":"Boîte de base",
            "paranoya_limit":3,
            "tags":[
                "masculin",
                "feminin"
            ],
            "faveurs":[
                {
                    "cost":3,
                    "effect":"Une fois par boucle, révélez le coupable d'un incident."
                },
                {
                    "cost":5,
                    "effect":"-1 Intrigue sur ce lieu ou à un personnage sur ce lieu."
                }
            ],
            "additionnal_field":"Entre en jeu à la boucle #",
            "additionnal_rule_list":[
                {
                    "step":"boucle_debut",
                    "condition":"",
                    "description":"Entre en jeu lors de la boucle définie dans le scénario."
                }
            ]
        },
        "policier":{
            "name":"Policier",
            "image_css_class":"card_policier",
            "board_game_box_origine":"Boîte de base",
            "paranoya_limit":3,
            "tags":[
                "adulte",
                "masculin"
            ],
            "faveurs":[
                {
                    "cost":4,
                    "effect":"Une fois par boucle, révélez le coupable d'un incident survenu lors de cette boucle."
                },
                {
                    "cost":5,
                    "effect":"Une fois par boucle, +1 Marqueur sur un personnage dans ce lieu. Si le personnage marqué doit mourir, retirer ce Marqueur à la place."
                }
            ],
            "additionnal_rule_list":[]
        },
        "employe_de_bureau":{
            "name":"Employé de bureau",
            "image_css_class":"card_employe_de_bureau",
            "board_game_box_origine":"Boîte de base",
            "paranoya_limit":2,
            "tags":[
                "adulte",
                "masculin"
            ],
            "faveurs":[
                {
                    "cost":3,
                    "effect":"Révélez le rôle de ce personnage."
                }
            ],
            "additionnal_rule_list":[]
        },
        "informatrice":{
            "name":"Informatrice",
            "image_css_class":"card_informatrice",
            "board_game_box_origine":"Boîte de base",
            "paranoya_limit":3,
            "tags":[
                "adulte",
                "feminin"
            ],
            "faveurs":[
                {
                    "cost":5,
                    "effect":"Une foi par boucle, le leadeur nomme le titre d'une machination secondaire. Le Mastermind révèle une autre machination secondaire active."
                }
            ],
            "additionnal_rule_list":[]
        },
        "pop_star":{
            "name":"Pop star",
            "image_css_class":"card_pop_star",
            "board_game_box_origine":"Boîte de base",
            "paranoya_limit":2,
            "tags":[
                "etudiant",
                "masculin"
            ],
            "faveurs":[
                {
                    "cost":3,
                    "effect":"-1 Paranoïa à un autre personnage dans ce lieu."
                },
                {
                    "cost":4,
                    "effect":"+1 Faveur à un autre personnage dans ce lieu."
                }
            ],
            "additionnal_rule_list":[]
        },
        "journaliste":{
            "name":"Journaliste",
            "image_css_class":"card_journaliste",
            "board_game_box_origine":"Boîte de base",
            "paranoya_limit":2,
            "tags":[
                "adulte",
                "masculin"
            ],
            "faveurs":[
                {
                    "cost":2,
                    "effect":"+1 Paranoïa à un autre personnage."
                },
                {
                    "cost":2,
                    "effect":"+1 Intrigue sur ce lieu ou à un personnage dans ce lieu."
                }
            ],
            "additionnal_rule_list":[]
        },
        "boss":{
            "name":"Boss",
            "image_css_class":"card_boss",
            "board_game_box_origine":"Boîte de base",
            "paranoya_limit":4,
            "tags":[
                "adulte",
                "masculin"
            ],
            "faveurs":[
                {
                    "cost":5,
                    "effect":"Une fois par boucle, révélez le rôle d'un autre personnage dans le territoire du boss."
                }
            ],
            "additionnal_field":"Territoire : #",
            "additionnal_rule_list":[
                {
                    "step":"passif",
                    "condition":"",
                    "description":"Pour toutes ses capacités (pas les Incidents), le Boss peut toujours être considéré comme étant dans on territoire."
                }
            ]
        },
        "medecin":{
            "name":"Médecin",
            "image_css_class":"card_medecin",
            "board_game_box_origine":"Boîte de base",
            "paranoya_limit":2,
            "tags":[
                "adulte",
                "masculin"
            ],
            "faveurs":[
                {
                    "cost":2,
                    "effect":"-/+ Paranoïa à un autre personnage dans ce lieu. Si le rôle du médecin a Refus de faveur, le mastermind peut utiliser cette capacité."
                },
                {
                    "cost":3,
                    "effect":"Le Patient peut sortir de l'hôpital jusqu'à la fin de la boucle."
                }
            ],
            "additionnal_rule_list":[]
        },
        "patient":{
            "name":"Patient",
            "image_css_class":"card_patient",
            "board_game_box_origine":"Boîte de base",
            "paranoya_limit":2,
            "tags":[
                "masculin"
            ],
            "faveurs":[],
            "additionnal_rule_list":[]
        },
        "infirmiere":{
            "name":"Infirmière",
            "image_css_class":"card_infirmiere",
            "board_game_box_origine":"Boîte de base",
            "paranoya_limit":3,
            "tags":[
                "adulte",
                "feminin"
            ],
            "faveurs":[
                {
                    "cost":2,
                    "effect":"(Cette capacité est immunisée au refus de faveur.) -1 Paranoïa à un autre personnage en état de panique (limite de Paranoïa atteinte) dans ce lieu."
                }
            ],
            "additionnal_rule_list":[]
        },
        "sbire":{
            "name":"Sbire",
            "image_css_class":"card_sbire",
            "board_game_box_origine":"Boîte de base",
            "paranoya_limit":1,
            "tags":[
                "adulte",
                "masculin"
            ],
            "faveurs":[
                {
                    "cost":3,
                    "effect":"Ce personnage ne peut plus déclencher d'incident lors de cette boucle."
                }
            ],
            "additionnal_rule_list":[
                {
                    "step":"boucle_debut",
                    "condition":"",
                    "description":"Le Mastermind choisit le lieu de départ de ce personnage."
                }
            ]
        },
        "scientifique":{
            "name":"Scientifique",
            "image_css_class":"card_scientifique",
            "board_game_box_origine":"Mystery Circle",
            "paranoya_limit":2,
            "tags":[
                "adulte",
                "masculin"
            ],
            "faveurs":[
                {
                    "cost":3,
                    "effect":"Retirez tous les jetons de ce personnage. Puis, si la jauge Extra est utilisée dans ce script, augmentez ou diminuez cette jauge."
                }
            ],
            "additionnal_rule_list":[
                {
                    "step":"boucle_debut",
                    "condition":"",
                    "description":"Le Mastermind place 1 Paranoïa, 1 Faveur ou 1 Intrigue sur ce personnage."
                }
            ]
        },
        "legiste":{
            "name":"Légiste",
            "image_css_class":"card_legiste",
            "board_game_box_origine":"Mystery Circle",
            "paranoya_limit":3,
            "tags":[
                "adulte",
                "masculin"
            ],
            "faveurs":[
                {
                    "cost":2,
                    "effect":"Une fois par boucle, déplacez un jeton entre 2 autres personnages de ce lieu."
                },
                {
                    "cost":5,
                    "effect":"Une fois par boucle, révélez le rôle d'un cadavre."
                }
            ],
            "additionnal_rule_list":[]
        },
        "intelligence_artificielle":{
            "name":"Intelligence Artificielle",
            "image_css_class":"card_intelligence_artificielle",
            "board_game_box_origine":"Mystery Circle",
            "paranoya_limit":4,
            "tags":[
                "creation"
            ],
            "faveurs":[
                {
                    "cost":3,
                    "effect":"Une fois par boucle, déplacez un jeton entre 2 autres personnages de ce lieu."
                }
            ],
            "additionnal_rule_list":[
                {
                    "step":"incidents",
                    "condition":"",
                    "description":"Quand on détermine si un incident, dont ce personnage est le coupable, se déclenche, tous les jetons de ce personnage comptent comme des jetons Paranoïa."
                }
            ]
        },
        "illusion":{
            "name":"Illusion",
            "image_css_class":"card_illusion",
            "board_game_box_origine":"Mystery Circle",
            "paranoya_limit":3,
            "tags":[
                "creation",
                "feminin"
            ],
            "faveurs":[
                {
                    "cost":3,
                    "effect":"Une fois par boucle, déplacez un personnage de ce lieu vers un autre lieu."
                },
                {
                    "cost":4,
                    "effect":"Une fois par boucle, retirez ce personnage du plateau pour le reste de la boucle."
                }
            ],
            "additionnal_rule_list":[
                {
                    "step":"actions_mastermind",
                    "condition":"",
                    "description":"Aucune carte action ne peut être placée sur ce personnage."
                },
                {
                    "step":"actions_protagonistes",
                    "condition":"",
                    "description":"Aucune carte action ne peut être placée sur ce personnage."
                },
                {
                    "step":"resolution_cartes",
                    "condition":"",
                    "description":"Toutes les cartes placées sur ce lieu sont aussi appliquées à ce personnage."
                }
            ]
        },
        "enseignante":{
            "name":"Enseignante",
            "image_css_class":"card_enseignante",
            "board_game_box_origine":"Cosmic Evil",
            "paranoya_limit":2,
            "tags":[
                "adulte",
                "feminin"
            ],
            "faveurs":[
                {
                    "cost":3,
                    "effect":"+/-1 Paranoïa à un étudiant dans ce lieu."
                },
                {
                    "cost":4,
                    "effect":"Une fois par boucle, révélez le rôle d'un étudiant dans ce lieu."
                }
            ],
            "additionnal_rule_list":[]
        },
        "etudiante_transferee":{
            "name":"Étudiante transférée",
            "image_css_class":"card_etudiante_transferee",
            "board_game_box_origine":"Cosmic Evil",
            "paranoya_limit":2,
            "tags":[
                "etudiant",
                "feminin"
            ],
            "faveurs":[
                {
                    "cost":2,
                    "effect":"Changez 1 Intrigue sur un personnage dans ce lieu en 1 Faveur."
                }
            ],
            "additionnal_field":"Arrive sur le plateau le jour #",
            "additionnal_rule_list":[
                {
                    "step":"journee_debut",
                    "condition":"",
                    "description":"Apparaît sur le plateau de jeu seulement au jour indiqué dans le scénario."
                }
            ]
        },
        "soldat":{
            "name":"Soldat",
            "image_css_class":"card_soldat",
            "board_game_box_origine":"Cosmic Evil",
            "paranoya_limit":3,
            "tags":[
                "adulte",
                "masculin"
            ],
            "faveurs":[
                {
                    "cost":2,
                    "effect":"Une fois par boucle, +2 Paranoïa sur un autre personnage dans ce lieu."
                },
                {
                    "cost":5,
                    "effect":"Une fois par boucle, les Protagonistes ne peuvent pas mourir pour le reste de cette boucle."
                }
            ],
            "additionnal_rule_list":[]
        },
        "chat_noir":{
            "name":"Chat noir",
            "image_css_class":"card_chat_noir",
            "board_game_box_origine":"Cosmic Evil",
            "paranoya_limit":0,
            "tags":[
                "animal"
            ],
            "faveurs":[],
            "additionnal_rule_list":[
                {
                    "step":"boucle_debut",
                    "condition":"",
                    "description":"+1 Intrigue sur le Sanctuaire."
                },
                {
                    "step":"incidents",
                    "condition":"",
                    "description":"L'effet d'un incident dont ce personnage est le coupable devient \"sans effet\"."
                }
            ]
        },
        "petite_fille":{
            "name":"Petite fille",
            "image_css_class":"card_petite_fille",
            "board_game_box_origine":"Toung Girl",
            "paranoya_limit":1,
            "tags":[
                "etudiant",
                "feminin"
            ],
            "faveurs":[
                {
                    "cost":1,
                    "effect":"Ce personnage peut sortir de l'école."
                },
                {
                    "cost":3,
                    "effect":"Une fois par boucle, déplacez ce personnage dans un lieu adjacent."
                }],
            "additionnal_rule_list":[]
        }
    },
    "role_list":{
        "personnage_cle": {
            "name": "Personnage clé",
            "refus_faveur": "",
            "description": "[Obligatoire : Si ce personnage décède] Les protagonistes perdent et la boucle se termine immédiatement.",
            "capacites": []
        },
        "tueur": {
            "name": "Tueur",
            "refus_faveur": "Facultatif",
            "description": "",
            "capacites": [{
                    "step": "journee_fin",
                    "condition": "Facultatif",
                    "description": "Si le <strong>Personnage clé</strong> a 2 Intrigues ou plus et se trouve dans le même lieu que le Tueur, le Personnage clé décède."
                }, {
                    "step": "journee_fin",
                    "condition": "Facultatif",
                    "description": "Si ce personnage a 4 Intrigues ou plus, les Protagonistes décèdent."
                }
            ]
        },
        "cerveau": {
            "name": "Cerveau",
            "refus_faveur": "Facultatif",
            "description": "",
            "capacites": [{
                    "step": "capacites_mastermind",
                    "condition": "Facultatif",
                    "description": "+1 Intrigue sur ce lieu ou sur un personnage dans ce lieu."
                }
            ]
        },
        "cultiste": {
            "name": "Cultiste",
            "refus_faveur": "Obligatoire",
            "description": "",
            "capacites": [{
                    "step": "resolution_cartes",
                    "condition": "Facultatif",
                    "description": "Vous pouvez ignorer tous les effets des cartes Pas d'intrigues sur ce lieu ou sur les personnages dans ce lieu."
                }
            ]
        },
        "conspirationniste": {
            "name": "Conspirationniste",
            "refus_faveur": "",
            "description": "",
            "max_count": 1,
            "capacites": [{
                    "step": "capacites_mastermind",
                    "condition": "Facultatif",
                    "description": "+1 Paranoïa à un personnage dans ce lieu."
                }
            ]
        },
        "tueur_en_serie": {
            "name": "Tueur en série",
            "refus_faveur": "",
            "description": "",
            "capacites": [{
                    "step": "journee_fin",
                    "condition": "Obligatoire",
                    "description": "Si exactement 1 autre personnage est dans ce lieu, cet autre personnage décède."
                }
            ]
        },
        "grincheux": {
            "name": "Grincheux",
            "refus_faveur": "Facultatif",
            "description": "",
            "capacites": []
        },
        "ami": {
            "name": "Ami",
            "refus_faveur": "",
            "description": "",
            "max_count": 2,
            "capacites": [{
                    "step": "boucle_fin",
                    "condition": "Obligatoire",
                    "description": "Si ce personnage est décédé, révélez son rôle et les Protagonistes perdent."
                }, {
                    "step": "boucle_debut",
                    "condition": "Obligatoire",
                    "description": "Si ce rôle a été révélé, +1 Faveur à ce personnage."
                }
            ]
        },
        "voyageur_temporel": {
            "name": "Voyageur temporel",
            "refus_faveur": "",
            "description": "<strong>Immortel</strong>",
            "capacites": [{
                    "step": "resolution_cartes",
                    "condition": "Obligatoire",
                    "description": "Ignorez les effets Pas de faveur sur ce personnage."
                }, {
                    "step": "journee_fin",
                    "condition": "Obligatoire",
                    "description": "Si c'est le dernier jour de la boucle et qu'il y a 2 Faveurs ou moins sur ce personnage, les Protagonistes perdrent et la boucle se termine."
                }
            ]
        },
        "sorciere": {
            "name": "Sorcière",
            "refus_faveur": "Obligatoire",
            "description": "",
            "capacites": []
        },
        "amoureux": {
            "name": "L'Amoureux",
            "refus_faveur": "",
            "description": "[Obligatoire, Si l'Être aimé décède] +6 Paranoïa à ce personnage.",
            "capacites": []
        },
        "etre_aime": {
            "name": "L'Être aimé",
            "refus_faveur": "",
            "description": "[Obligatoire, Si l'Amoureux décède] +6 Paranoïa à ce personnage.",
            "capacites": [{
                    "step": "journee_fin",
                    "condition": "Facultatif",
                    "description": "Si ce Personnage à 3 Paranoïa ou plus, et 1 Intrigue ou plus, les Protagonistes décèdent."
                }
            ]
        },
        "agent_dormant": {
            "name": "Agent dormant",
            "refus_faveur": "",
            "description": "[Obligatoire : Si ce personnage décède et qu'il y a 2 Intrigues ou plus sur le Centre-Ville] Les protagonistes perdent et la boucle se termine immédiatement.",
            "capacites": [{
                    "step": "capacites_mastermind",
                    "condition": "Facultatif, si 2 Intrigues ou plus sur le Lycée",
                    "description": "+1 Paranoïa à un personnage dans ce lieu (en copiant la capacité du conspirationniste)."
                }
            ]
        },
        "empoisonneur": {
            "name": "Empoisonneur",
            "refus_faveur": "Facultatif",
            "description": "",
            "capacites": [{
                    "step": "journee_fin",
                    "condition": "Obligatoire",
                    "description": "Une fois par boucle, si la jauge Extra est à 2 ou plus, un personnage dans ce lieu décède."
                }, {
                    "step": "journee_fin",
                    "condition": "Obligatoire",
                    "description": "Si la jauge Extra est à 4 ou plus, les Protagonistes décèdent."
                }
            ]
        },
        "fou": {
            "name": "Fou",
            "refus_faveur": "",
            "description": "",
            "max_count": 1,
            "capacites": [{
                    "step": "creation_script",
                    "condition": "",
                    "description": "Ce personnage doit être le coupable d'un Incident."
                }, {
                    "step": "incident",
                    "condition": "Obligatoire",
                    "description": "Après que ce personnage ait déclenché un Incident, retirez tous les jetons Paranoïa de sa carte."
                }
            ]
        },
        "detective_prive": {
            "name": "Détective privé",
            "refus_faveur": "",
            "description": "<strong>Immortel</strong>",
            "capacites": [{
                    "step": "creation_script",
                    "condition": "",
                    "description": "Ce personnage ne peut pas être le coupable d'un Incident."
                }, {
                    "step": "incident",
                    "condition": "Obligatoire",
                    "description": "Si la jauge Extra est à 0, et que le coupable est dans ce lieu, l'Incident se déclenche peu importe le nombre de jetons Paranoïa."
                }
            ]
        },
        "paranoiaque": {
            "name": "Paranoïaque",
            "refus_faveur": "Obligatoire",
            "description": "",
            "max_count": 1,
            "capacites": [{
                    "step": "capacites_mastermind",
                    "condition": "Facultatif",
                    "description": "+1 Paranoïa ou +1 Intrigue sur ce personnage."
                }
            ]
        },
        "jumeau": {
            "name": "Jumeau",
            "refus_faveur": "",
            "description": "",
            "capacites": [{
                    "step": "creation_script",
                    "condition": "",
                    "description": "Ce personnage doit être le coupable d'un Incident."
                }, {
                    "step": "incident",
                    "condition": "Obligatoire",
                    "description": "Quand ce personnage déclenche un Incident, il est considéré comme étant dans le lieu diagonalement opposé."
                }
            ]
        },
        "obstine": {
            "name": "Obstiné",
            "refus_faveur": "Obligatoire",
            "description": "",
            "capacites": [{
                    "step": "creation_script",
                    "condition": "",
                    "description": "Ce personnage doit être le coupable d'un Incident."
                }, {
                    "step": "incident",
                    "condition": "Obligatoire",
                    "description": "S'il et vivant, ce personnage déclenche toujours son Incident quelque soit le nombre de jetons Paranoïa sur lui."
                }
            ]
        },
        "psychiatre": {
            "name": "Psychiatre",
            "refus_faveur": "",
            "description": "",
            "capacites": [{
                    "step": "capacites_mastermind",
                    "condition": "Obligatoire",
                    "description": "Si la jauge Extra est à 1 ou plus, -1 Paranoïa sur un autre personnage dans ce lieu."
                }
            ]
        },
        "magicien": {
            "name": "Magicien",
            "refus_faveur": "",
            "description": "[Obligatoire : Si ce personnage décède] Retirez tous les jetons Paranoïa de son cadavre.",
            "capacites": [
                {
                    "step": "capacites_mastermind",
                    "condition": "Facultatif",
                    "description": "Une fois par boucle, vous pouvez déplacer un personnage avec 1 Paranoïa ou plus vers un lieu adjacent (pas en diagonale)."
                }
            ]
        },
        "ninja": {
            "name": "Ninja",
            "refus_faveur": "Facultatif",
            "description": "[Optionnel, à la révélation de ce rôle] Vous pouvez, plutôt que de dire la vérité, déclarer un autre rôle (pas Individu) utilisé dans ce script.",
            "capacites": [
                {
                    "step": "journee_fin",
                    "condition": "Facultatif",
                    "description": "Un personnage avec 2 Intrigues ou plus dans ce lieu décède."
                }
            ]
        },
        "prophete": {
            "name": "Prochète",
            "refus_faveur": "",
            "description": "",
            "capacites": [
                {
                    "step": "actions_mastermind",
                    "condition": "Obligatoire",
                    "description": "Le Mastermind ne peut pas placer de carte sur ce personnage."
                },
                {
                    "step": "incidents",
                    "condition": "Obligatoire",
                    "description": "Si le coupable est un autre personnage dans ce lieu, cet incident ne se déclenche pas, quelque soit la Paranoïa."
                }
            ]
        },
        "immortel": {
            "name": "Immortel",
            "refus_faveur": "",
            "description": "<strong>Immortel</strong>",
            "capacites": []
        }
    },
    "incident_list":{
        "meurtre": {
            "name": "Meurtre",
            "description": "Un autre personnage qui se trouve dans le même lieu que le coupable décède."
        },
        "malaise_grandissant": {
            "name": "Malaise grandissant",
            "description": "+2 Paranoïa à un personnage et +1 Intrigue à un autre."
        },
        "suicide": {
            "name": "Suicide",
            "description": "Le coupable décède."
        },
        "incident_a_l_hopital": {
            "name": "Incident à l'Hôpital",
            "description": "Si 1 Intrigue ou plus sur l'Hôpital, tous les personnages dans l'Hôpital décèdent. \nSi 2 Intrigues ou plus sur l'Hôpital, les protagonistes décèdent."
        },
        "meurtre_distant": {
            "name": "Meurtre distant",
            "description": "Un personnage avec 2 Intrigues ou plus décède."
        },
        "personne_disparue": {
            "name": "Personne disparue",
            "description": "Déplacez le coupable vers un lieu de votre choix. Ensuite, +1 Intrigue sur ce lieu."
        },
        "contagion": {
            "name": "Contagion",
            "description": "-2 Faveur à un personnage de votre choix. Ensuite, +2 Faveur à un autre personnage de votre choix."
        },
        "mal_odieux": {
            "name": "Mal Odieux",
            "description": "+2 Intrigues sur le Sanctuaire."
        },
        "effet_papillon": {
            "name": "Effet Papillon",
            "description": "Placez un jeton de votre choix sur un personnage qui se trouve dans le même lieu que le coupable."
        },
        "presage": {
            "name": "Présage",
            "description": "La limie de Paranoïa est diminuée de 1 pour cet Incident. +1 Paranoïa sur un personnage du lieu du coupable."
        },
        "terrorisme": {
            "name": "Terrorisme",
            "description": "Si au moins 1 Intrigue en Centre-ville : tous les personnage en Centre-ville décèdent. Si au moins 2 Intrigue en Centre-ville : les Protagonistes décèdent."
        },
        "meurtre_bestial": {
            "name": "Meurtre bestial",
            "description": "La limie de Paranoïa est augmentée de 1 pour cet Incident. Résolvez Meurtre et Malaise grandissant dans cet ordre. Puis augmentez la jaude Extra de 1."
        },
        "un_courrier_suspect": {
            "name": "Un courrier suspect",
            "description": "Déplacez un personnage du lieu du coupable vers un lieu. Si ce personnage change de lieu, il ne peut pas se déplacer le jour suivant."
        },
        "faux_suicide": {
            "name": "Faux suicide",
            "description": "Ajoutez une carte Extra sur le coupable. Les Protagonistes ne peuvent jouer aucune carte sur un personnage possédant une ou plusieurs carte Extra, jusqu'à la fin de la boucle."
        },
        "cercle_prive": {
            "name": "Cercle privé",
            "description": "Révélez le lieu du coupable. Pendant 3 jours (en incluant celui de l'incident), les déplacement depuis ou vers ce lieu sont annulés."
        },
        "la_balle_en_argent": {
            "name": "La balle en argent",
            "description": "La boucle se termine après la phase Incidents. Cet Incidetn n'augmente pas la jauge Extra. Vérifiez la victoire/défaite."
        },
        "tumulte": {
            "name": "Tumulte",
            "description": "Si au moins 1 Intrigue au Lycée : tous les personnages au Lycée décèdent. Si au moins 1 Intrigue au Centre-ville : tous les personnages au Centre-ville décèdent."
        },
        "incident_truque": {
            "name": "Incident truqué",
            "description": "S'il y a 2 Intrigues ou plus sur le lieu de départ du coupable, les Protagonistes décèdent. [Optionnel, Création du script] Le nom de l'incident sur la carte infos publiques est celui d'un autre incident."
        },
        "decouverte_capitale": {
            "name": "Découverte capitale",
            "description": "Le Leader choisit un lieu ou un personnage, et y retire 2 Intrigues."
        },
        "confession": {
            "name": "Confession",
            "description": "Révélez le coupable et son rôle."
        },
        "conspirations": {
            "name": "Conspirations",
            "description": "[Utilisez l'Intrigue à la place de la Paranoïa pour déclencher l'Incident] Résolvez l'incident Meurte ou Personne disparue."
        }
    },
    "tragedy_sets":{
        "first_steps":{
            "name":"Premiers pas",
            "description":"Version simplifiée du recueil de base pour apprendre à jouer. C'est un recueil d'introduction.<br/><strong>Règles spéciales</strong> : il n'y a pas de déduction finale et il n'y a qu'une seule machination secondaire.",
            "machination_primary":{
                "meurtre_premedite":{
                    "name":"Meurtre prémédité",
                    "description":"",
                    "role_config":{
                        "personnage_cle":1,
                        "tueur":1,
                        "cerveau":1
                    },
                    "additionnal_rule_list":[]
                },
                "lumiere_vengeresse":{
                    "name":"Lumière vengeresse",
                    "description":"[Condition de défaite : Fin de boucle] 2 Intrigues ou plus sur le lieu de départ du cerveau.",
                    "role_config":{
                        "cerveau":1
                    },
                    "additionnal_rule_list":[
                        {
                            "step":"boucle_fin",
                            "condition":"MP Lumière vengeresse",
                            "description":"2 Intrigues ou plus sur le lieu de départ du cerveau."
                        }
                    ]
                },
                "un_lieu_a_proteger":{
                    "name":"Un lieu à protéger",
                    "description":"[Condition de défaite : Fin de boucle] 2 Intrigues ou plus sur le Lycée.",
                    "role_config":{
                        "personnage_cle":1,
                        "cultiste":1
                    },
                    "additionnal_rule_list":[
                        {
                            "step":"boucle_fin",
                            "condition":"MP Un lieu à protéger",
                            "description":"2 Intrigues ou plus sur le Lycée."
                        }
                    ]
                }
            },
            "machination_secondary":{
                "l_ombre_de_leventreur":{
                    "name":"L'ombre de l'éventreur",
                    "description":"",
                    "role_config":{
                        "conspirationniste":1,
                        "tueur_en_serie":1
                    },
                    "additionnal_rule_list":[]
                },
                "rumeur_troublante":{
                    "name":"Rumeur troublante",
                    "description":"[Capacité du Mastermind] Une fois par boucle, vous pouvez placer 1 Intrigue sur le lieu de votre choix.",
                    "role_config":{
                        "conspirationniste":1
                    },
                    "additionnal_rule_list":[
                        {
                            "step":"capacites_mastermind",
                            "condition":"Facultatif, Une fois par boucle",
                            "description":"Vous pouvez placer 1 Intrigue sur le lieu de votre choix."
                        }
                    ]
                },
                "un_script_terrifiant":{
                    "name":"Un script terrifiant",
                    "description":"[Création du script] L'auteur du script peut choisir 0, 1 ou 2 Grincheux.",
                    "role_config":{
                        "conspirationniste":1,
                        "grincheux":2,
                        "ami":1
                    },
                    "additionnal_rule_list":[]
                }
            },
            "incident_list":[
                "meurtre",
                "malaise_grandissant",
                "suicide",
                "incident_a_l_hopital",
                "meurtre_distant",
                "personne_disparue",
                "contagion"
            ]
        },
        "base_tragedy":{
            "name":"Tragédies de base",
            "description":"Ensembe de tragédies basique. Aucune règle particulière n'est indiquée.",
            "machination_primary":{
                "meurtre_premedite":{
                    "name":"Meurtre prémédité",
                    "description":"",
                    "role_config":{
                        "personnage_cle":1,
                        "tueur":1,
                        "cerveau":1
                    },
                    "additionnal_rule_list":[]
                },
                "l_objet_scelle":{
                    "name":"L'Objet scellé",
                    "description":"[Condition de défaite : Fin de boucle] 2 Intrigues ou plus sur le Sanctuaire.",
                    "role_config":{
                        "cerveau":1,
                        "cultiste":1
                    },
                    "additionnal_rule_list":[
                        {
                            "step":"boucle_fin",
                            "condition":"MP L'Objet scellé",
                            "description":"2 Intrigues ou plus sur le Sanctuaire."
                        }
                    ]
                },
                "choisis_moi":{
                    "name":"Choisis moi !",
                    "description":"[Création du script] Le Personnage clé doit être une fille. [Condition de défaite : Fin de boucle] 2 Intrigues ou plus sur le Personnage clé.",
                    "role_config":{
                        "personnage_cle":1
                    },
                    "additionnal_rule_list":[
                        {
                            "step":"creation_script",
                            "condition":"",
                            "description":"Le Personnage clé doit être une fille."
                        },
                        {
                            "step":"boucle_fin",
                            "condition":"MP Choisis moi!",
                            "description":"2 Intrigues ou plus sur le Personnage clé."
                        }
                    ]
                },
                "futur_boulverse":{
                    "name":"Futur boulversé",
                    "description":"[Condition de défaite : Fin de boucle] L'incident Effet Papillon est survenu lors de cette boucle.",
                    "role_config":{
                        "cultiste":1,
                        "voyageur_temporel":1
                    },
                    "additionnal_rule_list":[
                        {
                            "step":"boucle_fin",
                            "condition":"MP Futur boulversé",
                            "description":"L'incident Effet Papillon est survenu lors de cette boucle."
                        }
                    ]
                },
                "bombe_temporelle":{
                    "name":"Bombe temporelle",
                    "description":"[Condition de défaite : Fin de boucle] 2 Intrigues ou plus sur le lieu de départ de la Sorcière.",
                    "role_config":{
                        "sorciere":1
                    },
                    "additionnal_rule_list":[
                        {
                            "step":"boucle_fin",
                            "condition":"MP Bombe temporelle",
                            "description":"2 Intrigues ou plus sur le lieu de départ de la Sorcière."
                        }
                    ]
                }
            },
            "machination_secondary":{
                "cercle_d_amis":{
                    "name":"Cercles d'amis",
                    "description":"",
                    "role_config":{
                        "ami":2,
                        "conspirationniste":1
                    },
                    "additionnal_rule_list":[]
                },
                "histoire_d_amour":{
                    "name":"Histoire d'amour",
                    "description":"",
                    "role_config":{
                        "amoureux":1,
                        "etre_aime":1
                    },
                    "additionnal_rule_list":[]
                },
                "obsede_cache":{
                    "name":"Obsédé caché",
                    "description":"",
                    "role_config":{
                        "ami":1,
                        "tueur_en_serie":1
                    },
                    "additionnal_rule_list":[]
                },
                "rumeur_troublante":{
                    "name":"Rumeur troublante",
                    "description":"[Capacité du Mastermind] Une fois par boucle, vous pouvez placer 1 Intrigue sur le lieu de votre choix.",
                    "role_config":{
                        "conspirationniste":1
                    },
                    "additionnal_rule_list":[
                        {
                            "step":"capacites_mastermind",
                            "condition":"Facultatif, Une fois par boucle",
                            "description":"Vous pouvez placer 1 Intrigue sur le lieu de votre choix."
                        }
                    ]
                },
                "paranoia_mortelle":{
                    "name":"Paranoïa mortelle",
                    "description":"[En tout temps] Chaque Individu (personnage sans rôle) avec 3 Paranoïa ou plus est considéré comme un Tueur en série.",
                    "role_config":{
                        "conspirationniste":1
                    },
                    "additionnal_rule_list":[
                        {
                            "step":"journee_fin",
                            "condition":"MS Paranoïa mortelle",
                            "description":"Chaque Individu (personnage sans rôle) avec 3 Paranoïa ou plus est considéré comme un Tueur en série."
                        }
                    ]
                },
                "fils_du_destin":{
                    "name":"Fils du destin",
                    "description":"[Début de boucle] +2 Paranoïa à chaque personnage qui, ou dont le cadavre avait 1 Faveur ou plus à la fin de la boucle précédente.",
                    "role_config":{},
                    "additionnal_rule_list":[
                        {
                            "step":"boucle_debut",
                            "condition":"MS Fils du destin",
                            "description":"+2 Paranoïa à chaque personnage qui, ou dont le cadavre avait 1 Faveur ou plus à la fin de la boucle précédente."
                        }
                    ]
                },
                "element_inconnu_x":{
                    "name":"Élément inconnu X",
                    "description":"",
                    "role_config":{
                        "agent_dormant":1
                    },
                    "additionnal_rule_list":[]
                }
            },
            "incident_list":[
                "meurtre",
                "malaise_grandissant",
                "mal_odieux",
                "suicide",
                "incident_a_l_hopital",
                "meurtre_distant",
                "personne_disparue",
                "contagion",
                "effet_papillon"
            ]
        },
        "mystery_circle":{
            "name":"Mystery Circle (Cercle Mystérieux)",
            "description":"<strong>Règle spéciale</strong><ul><li>Les cartes Extra s'enlèvent en même temps que les jetons au début d'une boucle.</li><li>La jauge Extra commence chaque boucle sur 0.</li><li>Pour chaque Incident qui se déclenche (même sans effet), augmentez la jauge Extra de 1.</li></ul>",
            "machination_primary":{
                "meurtre_premedite":{
                    "name":"Meurtre prémédité",
                    "description":"",
                    "role_config":{
                        "personnage_cle":1,
                        "tueur":1,
                        "cerveau":1
                    },
                    "additionnal_rule_list":[]
                },
                "un_plan_sur_corde_raide":{
                    "name":"Un plan sur corde raide",
                    "description":"[Condition de défaite : Fin de boucle] Si la jauge Extra est à 1 ou moins, les Protagonistes perdent.",
                    "role_config":{
                        "tueur":1,
                        "cerveau":1
                    },
                    "additionnal_rule_list":[
                        {
                            "step":"boucle_fin",
                            "condition":"MP Un plan sur la corde",
                            "description":"Si la jauge Extra est à 1 ou moins, les Protagonistes perdent."
                        }
                    ]
                },
                "une_goute_de_paison":{
                    "name":"Une goute de poison",
                    "description":"[Déclenchement des Incidents] Pour déterminer le déclenchement des Incodents Meurtre et Suicide, les jetons Intrigue comptent comme des jetons Paranoïa.",
                    "role_config":{
                        "fou":1,
                        "personnage_cle":1,
                        "empoisonneur":1
                    },
                    "additionnal_rule_list":[
                        {
                            "step":"incidents",
                            "condition":"",
                            "description":"Pour déterminer le déclenchement des Incodents Meurtre et Suicide, les jetons Intrigue comptent comme des jetons Paranoïa."
                        }
                    ]
                },
                "incidents_multiples":{
                    "name":"Incidents multiples",
                    "description":"[Condition de défaite : Fin de boucle] Si la jauge Extra est à 3 ou plus, les Protagonistes perdent.",
                    "role_config":{
                        "conspirationniste":1,
                        "fou":1
                    },
                    "additionnal_rule_list":[
                        {
                            "step":"boucle_fin",
                            "condition":"MP Incidents multiples",
                            "description":"Si la jauge Extra est à 3 ou plus, les Protagonistes perdent."
                        }
                    ]
                },
                "l_ecole_noire":{
                    "name":"L'école noire",
                    "description":"[Condition de défaite : Fin de boucle] S'il y a X jetons Intrigue sur le Lycée, les Protagonistes perdent. X = numéro de la boucle actuelle - 1 (Les Protagonistes perdent toujours la première boucle).",
                    "role_config":{
                        "cerveau":1
                    },
                    "additionnal_rule_list":[
                        {
                            "step":"boucle_fin",
                            "condition":"MP L'école noire",
                            "description":"S'il y a X jetons Intrigue sur le Lycée, les Protagonistes perdent. X = numéro de la boucle actuelle - 1 (Les Protagonistes perdent toujours la première boucle)."
                        }
                    ]
                }
            },
            "machination_secondary":{
                "obsede_cache":{
                    "name":"Obsédé caché",
                    "description":"",
                    "role_config":{
                        "ami":1,
                        "tueur_en_serie":1
                    },
                    "additionnal_rule_list":[]
                },
                "danse_des_fous":{
                    "name":"Danse des fous",
                    "description":"",
                    "role_config":{
                        "fou":1,
                        "ami":1
                    },
                    "additionnal_rule_list":[]
                },
                "psychopathe_interne":{
                    "name":"Psychopathe interné",
                    "description":"[Début de la boucle] Si la jauge Extra était à 2 ou moins à la fin de la boucle précédente, augmentez-la de 1.",
                    "role_config":{
                        "conspirationniste":1,
                        "psychiatre":1,
                        "paranoiaque":1
                    },
                    "additionnal_rule_list":[
                        {
                            "step":"boucle_debut",
                            "condition":"MS Psychopathe interné",
                            "description":"Si la jauge Extra était à 2 ou moins à la fin de la boucle précédente, augmentez-la de 1."
                        }
                    ]
                },
                "une_volonte_absolue":{
                    "name":"Une volonté absolue",
                    "description":"",
                    "role_config":{
                        "obstine":1
                    },
                    "additionnal_rule_list":[]
                },
                "dangereux_jumeaux":{
                    "name":"Dangereux jumeaux",
                    "description":"",
                    "role_config":{
                        "jumeau":1,
                        "paranoiaque":1
                    },
                    "additionnal_rule_list":[]
                },
                "odeur_de_poudre":{
                    "name":"Odeur de poudre",
                    "description":"[Fin de boucle] S'il y a un total de 12 jetons Paranoïa ou plus sur les personnages restant, les Protagonistes perdent",
                    "role_config":{
                        "tueur_en_serie":1
                    },
                    "additionnal_rule_list":[
                        {
                            "step":"boucle_fin",
                            "condition":"MS Odeur de poudre",
                            "description":"S'il y a un total de 12 jetons Paranoïa ou plus sur les personnages restant, les Protagonistes perdent."
                        }
                    ]
                },
                "je_suis_un_detective_expert":{
                    "name":"Je suis un détective expert",
                    "description":"",
                    "role_config":{
                        "conspirationniste":1,
                        "ami":1,
                        "detective_prive":1
                    },
                    "additionnal_rule_list":[]
                }
            },
            "incident_list":[
                "meurtre",
                "incident_a_l_hopital",
                "presage",
                "malaise_grandissant",
                "terrorisme",
                "meurtre_bestial",
                "suicide",
                "un_courrier_suspect",
                "faux_suicide",
                "cercle_prive",
                "la_balle_en_argent"
            ]
        },
        "midnight_zone":{
            "name":"Midnight Zone (Zone aphotique/Zone sombre)",
            "description":"<strong>Règle spéciale</strong> : Les cartes Extra s'enlèvent en même temps que les jetons au début d'une boucle.",
            "machination_primary":{
                "l_objet_scelle":{
                    "name":"L'Objet scellé",
                    "description":"[Condition de défaite : Fin de boucle] 2 Intrigues ou plus sur le Sanctuaire.",
                    "role_config":{
                        "cerveau":1,
                        "cultiste":1
                    },
                    "additionnal_rule_list":[
                        {
                            "step":"boucle_fin",
                            "condition":"MP L'Objet scellé",
                            "description":"2 Intrigues ou plus sur le Sanctuaire."
                        }
                    ]
                },
                "enregistrement_secret":{
                    "name":"Enregistrement secret",
                    "description":"[Condition de défaite : Fin de boucle] Si le Cerveau, l'Agent dormant ou le Magicien a été révélé, les Protagonistes perdent.",
                    "role_config":{
                        "conspirationniste":1,
                        "personnage_cle":1,
                        "cerveau":1
                    },
                    "additionnal_rule_list":[
                        {
                            "step":"boucle_fin",
                            "condition":"MP Enregistrement secret",
                            "description":"Si le Cerveau, l'Agent dormant ou le Magicien a été révélé, les Protagonistes perdent."
                        }
                    ]
                },
                "la_main_du_diable":{
                    "name":"La mains du Diable",
                    "description":"",
                    "role_config":{
                        "ninja":1,
                        "personnage_cle":1,
                        "cultiste":1
                    },
                    "additionnal_rule_list":[]
                },
                "combat_d_hommes":{
                    "name":"Combat d'hommes",
                    "description":"[Condition de défaite : Fin de boucle] Si le Ninja ou son cadavre a 2 Intrigue ou plus, les Protagonistes perdent.",
                    "role_config":{
                        "ninja":1
                    },
                    "additionnal_rule_list":[
                        {
                            "step":"creation_script",
                            "condition":"",
                            "description":"Le Ninja doit être un Homme (pas un garçon)."
                        },
                        {
                            "step":"boucle_fin",
                            "condition":"MP Combat d'hommes",
                            "description":"Si le Ninja ou son cadavre a 2 Intrigue ou plus, les Protagonistes perdent."
                        }
                    ]
                },
                "connections_malheureuses":{
                    "name":"Connections malheureuses",
                    "description":"[Condition de défaite : Début de boucle] Choisissez un personnage qui est mort durant la boucle précédente. Placez une carte Extra sur ce personnage. Le rôle des personnages avec une carte Extra devient Personnage clé.",
                    "role_config":{
                        "conspirationniste":1,
                        "tueur_en_serie":1,
                        "ami":1,
                    },
                    "additionnal_rule_list":[
                        {
                            "step":"boucle_debut",
                            "condition":"MP Connections malheureuses",
                            "description":"Choisissez un personnage qui est mort durant la boucle précédente. Placez une carte Extra sur ce personnage. Le rôle des personnages avec une carte Extra devient Personnage clé."
                        }
                    ]
                }
            },
            "machination_secondary":{
                "spirale_amoureuse":{
                    "name":"Spirale amoureuse",
                    "description":"",
                    "role_config":{
                        "ami":1,
                        "obstine":1
                    },
                    "additionnal_rule_list":[]
                },
                "spectacle_mortuaire":{
                    "name":"Spectable mortuaire",
                    "description":"[Condition de défaite, fin de la boucle] S'il y a 6 ou moins personnages vivants, les Portagonistes perdent.",
                    "role_config":{
                        "magicien":1,
                        "immortel":1
                    },
                    "additionnal_rule_list":[
                        {
                            "step":"boucle_fin",
                            "condition":"MS Spectable mortuaire",
                            "description":"S'il y a 6 ou moins personnages vivants, les Portagonistes perdent."
                        }
                    ]
                },
                "le_the_des_sorcieres":{
                    "name":"Le thé des sorcières",
                    "description":"",
                    "role_config":{
                        "conspirationniste":1,
                        "ami":1,
                        "sorciere":2
                    },
                    "additionnal_rule_list":[]
                },
                "les_des_des_dieux":{
                    "name":"Les dés des Dieux",
                    "description":"[Début de la boucle] Placez une carte Extra sur un personnage décédé lors de la boucle précédente.",
                    "role_config":{
                        "tueur_en_serie":1,
                        "obstine":1
                    },
                    "additionnal_rule_list":[
                        {
                            "step":"boucle_debut",
                            "condition":"MS Les dés des Dieux",
                            "description":"Placez une carte Extra sur un personnage décédé lors de la boucle précédente."
                        }
                    ]
                },
                "coeur_sans_reponse":{
                    "name":"Coeur sans réponse",
                    "description":"[Résolution des cartes] Les cartes Pas de faveur ont aussi l'effet Pas de mouvement.",
                    "role_config":{
                        "conspirationniste":1,
                        "magicien":1
                    },
                    "additionnal_rule_list":[
                        {
                            "step":"resolution_cartes",
                            "condition":"MS Coeur sans réponse",
                            "description":"Les cartes Pas de faveur ont aussi l'effet Pas de mouvement."
                        }
                    ]
                },
                "declencheur_risque":{
                    "name":"Déclencheur risqué",
                    "description":"",
                    "role_config":{
                        "agent_dormant":1
                    },
                    "additionnal_rule_list":[
                        {
                            "step":"capacites_mastermind",
                            "condition":"MS Déclencheur risqué",
                            "description":"Une fois par boucle, +1 Intrigue sur le lieu de l'Agent dormant (si vivant)."
                        }
                    ]
                },
                "les_temoins_de_l_apocalypse":{
                    "name":"Les témoins de l'apocalypse",
                    "description":"[Création du script] Il doit y avoir au moins un incident Suicide. [Déclenchement des incidents] Si le Prophète est vivant, et que le coupable est un Individu, la limite de Paranoïa du coupable est diminuée de 1.",
                    "role_config":{
                        "prophete":1
                    },
                    "additionnal_rule_list":[
                        {
                            "step":"incidents",
                            "condition":"MS Les témoins de l'apocalypse",
                            "description":"Si le Prophète est vivant, et que le coupable est un Individu, la limite de Paranoïa du coupable est diminuée de 1."
                        }
                    ]
                }
            },
            "incident_list":[
                "meurtre",
                "incident_a_l_hopital",
                "personne_disparue",
                "malaise_grandissant",
                "tumulte",
                "incident_truque",
                "suicide",
                "decouverte_capitale",
                "faux_suicide",
                "confession",
                "conspirations"
            ]
        }
    }
};
