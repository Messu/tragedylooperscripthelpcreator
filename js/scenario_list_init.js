
var existingScenarioList = [
    {
        "tragedySet": "first_steps",
        "title": "Template",
        "origine_id": "0",
        "origine_tag": "NA",
        "origine": "None",
        "informations": "<p>\n<strong>Créateur</strong> : AUTEUR<br />\n<strong>Boucles</strong> : N++ / <strong>Difficulté</strong> : D--<br />\n<strong>Boucles</strong> : N-- / <strong>Difficulté</strong> : D++<br />\n<strong>Jours par boucle</strong> : 4\n</p>\n<h4>Particularités</h4>\n<p>\nPLACEHOLDER\n</p>\n<h4>Histoire</h4>\n<p>\nPLACEHOLDER\n</p>\n<h4>Conseils pour le Mastermind</h4>\n<p>\nPLACEHOLDER\n</p>",
        "mastermind_victory_conditions": "<ol>\n    <li>\n        <strong>Tuer le Personnage clé (Lycéenne)</strong><br/>\n        La capacité du Tueur / la capacité du Tueur en série / l'incident Meutre / l'incident Suicide\n    </li>\n    <li>\n        <strong>Tuer les Protagonistes</strong><br/>\n        La capacité du Tueur\n    </li>\n</ol>",
        "machination_primary": "meurtre_premedite",
        "machination_secondary_list": [
            "l_ombre_de_leventreur"
        ],
        "character_list": [],
        "incident_list": []
    },
    {
        "tragedySet": "base_tragedy",
        "title": "Le Culte de la fille qui saute",
        "origine_id": "1",
        "origine_tag": "M",
        "origine": "Maison",
        "informations": "<h1>WIP</h1><br/>Autheur : Messu Kilkain<br />\nBoucles : 5<br />\nJours par boucle : 6",
        "mastermind_victory_conditions": "",
        "machination_primary": "futur_boulverse",
        "machination_secondary_list": [
            "obsede_cache",
            "element_inconnu_x"
        ],
        "character_list": [{
                "characterCode": "chat_noir",
                "roleCode": "cultiste"
            }, {
                "characterCode": "garcon_mysterieux",
                "roleCode": "personnage_cle"
            }, {
                "characterCode": "lyceen",
                "roleCode": "tueur_en_serie"
            }, {
                "characterCode": "riche_heritiere",
                "roleCode": "voyageur_temporel"
            }, {
                "characterCode": "patient",
                "roleCode": "ami"
            }, {
                "characterCode": "policier",
                "roleCode": ""
            }, {
                "characterCode": "employe_de_bureau",
                "roleCode": "agent_dormant"
            }, {
                "characterCode": "gardienne_du_sanctuaire",
                "roleCode": ""
            }
        ],
        "incident_list": [{
                "incidentCode": "effet_papillon",
                "jourNumero": "2",
                "coupable": "chat_noir"
            }, {
                "incidentCode": "meurtre",
                "jourNumero": "3",
                "coupable": "patient"
            }, {
                "incidentCode": "incident_a_l_hopital",
                "jourNumero": "4",
                "coupable": "lyceen"
            }, {
                "incidentCode": "suicide",
                "jourNumero": "5",
                "coupable": "employe_de_bureau"
            }, {
                "incidentCode": "personne_disparue",
                "jourNumero": "6",
                "coupable": "riche_heritiere"
            }
        ]
    }
];
