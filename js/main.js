
$(initialMainDataLoad);

function initialMainDataLoad() {
    $("#tragedy_set_select").empty();
    
    // Initialisation des roles des recueils de tragédie
    for(var tragedySetCode in mainData.tragedy_sets ) {
        var tragedySetData = mainData.tragedy_sets[tragedySetCode];
        var machinationRoleSet = new Set();
        for(var mpCode in tragedySetData.machination_primary) {
            for(var roleCode in tragedySetData.machination_primary[mpCode].role_config) {
                machinationRoleSet.add(roleCode);
            }
        }
        for(var mpCode in tragedySetData.machination_secondary) {
            for(var roleCode in tragedySetData.machination_secondary[mpCode].role_config) {
                machinationRoleSet.add(roleCode);
            }
        }
        var machinationRoleArray = Array.from(machinationRoleSet).sort();
        tragedySetData["role_list"] = {};
        for(var roleCodeIndex in machinationRoleArray) {
            var roleCode = machinationRoleArray[roleCodeIndex];
            if(undefined === mainData.role_list[roleCode]){
                console.error("Le recueil "+tragedySetCode+" possède le rôle "+roleCode+"qui n'a pas de description.");
            }
            tragedySetData["role_list"][roleCode] = mainData.role_list[roleCode];
        }
        var incidentList = tragedySetData.incident_list;
        tragedySetData.incident_list = {};
        for(var incidentCodeIndex in incidentList) {
            var incidentCode = incidentList[incidentCodeIndex];
            if(undefined === mainData.incident_list[incidentCode]){
                console.error("Le recueil "+tragedySetCode+" possède l'incident "+incidentCode+"qui n'a pas de description.");
            }
            tragedySetData.incident_list[incidentCode] = mainData.incident_list[incidentCode];
        }
    }
    for(var tragedySetCode in mainData.tragedy_sets ) {
        $("#tragedy_set_select").append($("<option>").val(tragedySetCode).text(mainData.tragedy_sets[tragedySetCode]["name"]));
    }
    loadNewTragedySet();
    for(var scenarioIndex in existingScenarioList ) {
        var scenario = existingScenarioList[scenarioIndex];
        $("#existing_scenario_select").append($("<option>").val(scenarioIndex).text(scenario["origine_tag"] + " - " + scenario["origine_id"] + " - " + scenario["title"]));
    }
}

function loadJsonOfExistingScenario() {
    var scenarioIndex = $("#existing_scenario_select").val();
    var textareaElement = $("textarea[name='export_import_textarea']").first();
    var scenarioData = existingScenarioList[scenarioIndex];
    textareaElement.val(JSON.stringify(scenarioData));
}

function loadNewTragedySet() {
    var newTragedySetCode = $("#tragedy_set_select").val();
    var newTragedySet = mainData.tragedy_sets[newTragedySetCode];
    $("#tragedy_set_current_description").html(newTragedySet["description"]);
    $("#machination_primary_select").empty();
    for( machinationPrimaryCode in newTragedySet["machination_primary"] ) {
        $("#machination_primary_select").append($("<option>").val(machinationPrimaryCode).text(newTragedySet["machination_primary"][machinationPrimaryCode]["name"]));
    }
    $("#machination_secondary_select").empty();
    for( machinationSecondaryCode in newTragedySet["machination_secondary"] ) {
        $("#machination_secondary_select").append($("<option>").val(machinationSecondaryCode).text(newTragedySet["machination_secondary"][machinationSecondaryCode]["name"]));
    }
    $("#character_select").empty();
    $("#character_select").append($("<option>"));
    for( characterCode in mainData.character_list ) {
        $("#character_select").append($("<option>").val(characterCode).text(
            mainData.character_list[characterCode]["name"]
            + " (" + mainData.character_list[characterCode]["board_game_box_origine"] + ")"
        ));
    }
    $("#incident_select").empty();
    for( incidentCode in newTragedySet["incident_list"] ) {
        $("#incident_select").append($("<option>").val(incidentCode).text(newTragedySet["incident_list"][incidentCode]["name"]));
    }
    resetAllSelection();
}

function setMachinationPrimary() {
    var tragedySetCode = $("#tragedy_set_select").val();
    var machinationPrimaryCode = $("#machination_primary_select").val();
    var newMachinationPrimary = mainData.tragedy_sets[tragedySetCode]["machination_primary"][machinationPrimaryCode];
    $("#machination_primary_selected").attr("data-code",machinationPrimaryCode)
        .html(newMachinationPrimary["name"]);
    recalculateRoleOccupation();
}

function addMachinationSecondary() {
    var tragedySetCode = $("#tragedy_set_select").val();
    var newMachinationSecondaryCode = $("#machination_secondary_select").val();
    var newMachinationSecondary = mainData.tragedy_sets[tragedySetCode]["machination_secondary"][newMachinationSecondaryCode];
    var newMachinationSecondaryElement = $("#machination_secondary_selected_template").clone();
    newMachinationSecondaryElement.appendTo("#machination_info_panel")
        .show()
        .attr("id", "")
        .attr("data-code", newMachinationSecondaryCode);
    newMachinationSecondaryElement.find(".machination_secondary_name")
        .html(newMachinationSecondary["name"])
        ;
    recalculateRoleOccupation();
    return newMachinationSecondaryElement;
}

function removeMachinationSecondary(sourceEventElement) {
    $(sourceEventElement).parents(".machination_secondary_selected").remove();
    recalculateRoleOccupation();
}

function addCharacter() {
    var tragedySetCode = $("#tragedy_set_select").val();
    var newCharacterCode = $("#character_select").val();
    if(
        undefined === newCharacterCode
        || null === newCharacterCode
        || "" === newCharacterCode
    ) {
        console.log("Can't add empty character");
        return;
    }
    var newCharacter = mainData.character_list[newCharacterCode];
    var newCharacterElement = $("#character_line_template").clone()
        .appendTo("#character_list_tbody")
        .show()
        .attr("id", "")
        .attr("data-code", newCharacterCode);
    //console.log(newCharacterElement.find(".character_image"));
    //console.log(newCharacter["image_css_class"]);
    newCharacterElement.find(".character_image").addClass(newCharacter["image_css_class"]);
    //console.log(newCharacter["name"]);
    //console.log(newCharacterElement.find(".character_name"));
    newCharacterElement.find(".character_name").html(newCharacter["name"]);
    if(undefined !== newCharacter["additionnal_field"] && '' !== newCharacter["additionnal_field"]) {
        newCharacterElement.find(".additionnal_field").show().val(newCharacter["additionnal_field"]);
    }
    
    var characterRoleSelect = newCharacterElement.find(".character_role_select");
    for(var roleCode in mainData.tragedy_sets[tragedySetCode]["role_list"] ) {
        characterRoleSelect.append($("<option>").val(roleCode).text(mainData.tragedy_sets[tragedySetCode]["role_list"][roleCode]["name"]));
    }
    checkIncidentValidity();
    $("#character_select option[value="+newCharacterCode+"]").attr('disabled','disabled');
    {
        var newValue = $("#character_select option[value="+newCharacterCode+"] + option:not([disabled])").first().attr("value");
        console.log("newValue",newValue);
        if(
            undefined === newValue
            || null === newValue
            || "" === newValue
        ) {
            newValue = $("#character_select option[value][value!='']:not([disabled])").first().attr("value");
        }
        console.log("newValue",newValue);
        $("#character_select").val(newValue);
    }
    return newCharacterElement;
}

function removeCharacter(sourceEventElement) {
    var characterCode = $(sourceEventElement).parents("tr").attr("data-code");
    $(sourceEventElement).parents("tr").remove();
    recalculateRoleOccupation();
    checkIncidentValidity();
    $("#character_select option[value="+characterCode+"]").removeAttr('disabled');
}

function addIncident() {
    var tragedySetCode = $("#tragedy_set_select").val();
    var newIncidentCode = $("#incident_select").val();
    var newIncidentJour = $("#incident_day_input").val();
    var newIncident = mainData.tragedy_sets[tragedySetCode]["incident_list"][newIncidentCode];
    var newIncidentElement = $("#incident_line_template").clone()
        .appendTo("#incident_list_tbody")
        .show()
        .attr("id", "")
        .attr("data-code", newIncidentCode);
    newIncidentElement.find(".incident_jour").attr("data-jour-numero",newIncidentJour).text(newIncidentJour);
    newIncidentElement.find(".incident_name").html(newIncident["name"]);
    
    var coupableSelect = newIncidentElement.find(".incident_coupable_select");
    for( characterCode in mainData.character_list ) {
        coupableSelect.append($("<option>").val(characterCode).text(mainData.character_list[characterCode]["name"]));
    }
    // Tri par jour.
    {
        var incidentRowList = $("#incident_list_tbody").find("tr[id!=incident_line_template]").toArray()
            .sort(function(a,b){
                var valA = $(a).find(".incident_jour").attr("data-jour-numero");
                var valB = $(b).find(".incident_jour").attr("data-jour-numero");
                return $.isNumeric(valA) && $.isNumeric(valB) ? valA - valB : valA.toString().localeCompare(valB);
            });
        for (var i = 0; i < incidentRowList.length; i++) {
            $("#incident_list_tbody").append(incidentRowList[i]);
        }
    }
    checkIncidentValidity();
    $("#incident_day_input").val(parseInt($("#incident_day_input").val())+1);
    return newIncidentElement;
}

function checkIncidentValidity() {
    // Le coupable doit être dans les personnages présents.
    // Aucun personnage ne peut être le coupable de deux incidents.
    // Plusieurs incidents d'un même type peuvent avoir lieu lors d'une même boucle.
    // Pas plus d'un incident ne peut avoir lieu un même jour.
    var existingCharacterSet = new Set();
    $("#character_list_tbody tr[id!=character_line_template]").each(function(index, element){
        existingCharacterSet.add($(element).attr("data-code"));
    });
    var existingDaySet = new Set();
    var issueDaySet = new Set();
    var existingGuiltyCharacterSet = new Set();
    var issueCharacterSet = new Set();
    var incidentLineList = $("#incident_list_tbody").find("tr[id!=incident_line_template]");
    incidentLineList.each(function(index, element) {
        $(element).removeClass("invalid");
        var jourNumero = $(element).find(".incident_jour").attr("data-jour-numero");
        if(existingDaySet.has(jourNumero)){
            issueDaySet.add(jourNumero);
        }
        var coupable = $(element).find(".incident_coupable_select").val();
        if(existingGuiltyCharacterSet.has(coupable)){
            issueCharacterSet.add(coupable);
        }
        existingDaySet.add(jourNumero);
        existingGuiltyCharacterSet.add(coupable);
    });
    incidentLineList.each(function(index, element) {
        var isInvalid = false;
        var jourNumero = $(element).find(".incident_jour").attr("data-jour-numero");
        isInvalid |= issueDaySet.has(jourNumero);
        var coupable = $(element).find(".incident_coupable_select").val();
        isInvalid |= issueCharacterSet.has(coupable);
        isInvalid |= ("" === coupable);
        isInvalid |= !existingCharacterSet.has(coupable);
        if(isInvalid) {
            $(element).addClass("invalid");
        }
    });
}

function removeIncident(sourceEventElement) {
    $(sourceEventElement).parents("tr").remove();
    checkIncidentValidity();
}

function addToRoleCounter(roleConfig, roleCode, roleValue) {
    if(undefined === roleConfig[roleCode]) {
        roleConfig[roleCode] = roleValue;
    } else {
        roleConfig[roleCode] += roleValue;
    }
    return roleConfig;
}

function recalculateRoleOccupation() {
    var tragedySetCode = $("#tragedy_set_select").val();
    var roleConfig = {};
    // Machination primaire
    {
        var machinationPrimaryCode = $("#machination_info_panel #machination_primary_selected").attr("data-code");
        var machinationPrimary = mainData.tragedy_sets[tragedySetCode]["machination_primary"][machinationPrimaryCode];
        if(undefined !== machinationPrimary) {
            for (var machinationPrimaryRoleConfig in machinationPrimary["role_config"]) {
                //console.log("machinationPrimaryRoleConfig", machinationPrimaryRoleConfig);
                //console.log("machinationPrimary[role_config][machinationPrimaryRoleConfig]", machinationPrimary["role_config"][machinationPrimaryRoleConfig]);
                roleConfig = addToRoleCounter(
                    roleConfig,
                    machinationPrimaryRoleConfig, // roleCode,
                    machinationPrimary["role_config"][machinationPrimaryRoleConfig] // roleValue
                );
            }
        }
    }
    // Machination(s) secondaire(s)
    {
        $("#machination_info_panel .machination_secondary_selected[id!=machination_secondary_selected_template]")
            .each(function(index, element) {
            var machinationSecondaryCode = $(element).attr("data-code");
            var machinationSecondary = mainData.tragedy_sets[tragedySetCode]["machination_secondary"][machinationSecondaryCode];
            for (var machinationSecondaryRoleConfig in machinationSecondary["role_config"]) {
                //console.log("machinationSecondaryRoleConfig", machinationSecondaryRoleConfig);
                //console.log("machinationSecondary[role_config][machinationSecondaryRoleConfig]", machinationSecondary["role_config"][machinationSecondaryRoleConfig]);
                roleConfig = addToRoleCounter(
                    roleConfig,
                    machinationSecondaryRoleConfig, // roleCode,
                    machinationSecondary["role_config"][machinationSecondaryRoleConfig] // roleValue
                );
            }
        });
    }
    // Personnage(s)
    {
        $("#character_list_tbody tr[id!=character_line_template] td select.character_role_select option:selected")
            .each(function(index, element) {
            var roleCode = $(element).val();
            if( undefined !== roleCode && '' !== roleCode ) {
                roleConfig = addToRoleCounter(
                    roleConfig,
                    roleCode,
                    0
                );
            }
        });
    }
    // Application du max spécifique aux rôles
    for (var roleCode in roleConfig) {
        var roleData = mainData.tragedy_sets[tragedySetCode]["role_list"][roleCode];
        if(undefined !== roleData["max_count"]) {
            roleConfig[roleCode] = Math.min(roleConfig[roleCode], roleData["max_count"]);
        }
    }
    //console.log("roleConfig", roleConfig)
    // Build new role occupation display
    $("#role_occupation_info_panel > span[id!=role_counter_template]").remove();
    var templateElement = $("#role_occupation_info_panel span[id=role_counter_template]");
    for (var roleCode in roleConfig) {
        var roleData = mainData.tragedy_sets[tragedySetCode]["role_list"][roleCode];
        var newElement = templateElement.clone().attr("id", "");
        newElement.show();
        newElement.find(".role_name").text(roleData["name"]);
        var countCurrent = $("#character_list_tbody tr[id!=character_line_template] td select.character_role_select option:selected[value="+roleCode+"]").length;
        var countMax = roleConfig[roleCode];
        newElement.find(".role_count_current").text(countCurrent);
        newElement.find(".role_count_max").text(countMax);
        if(countCurrent === countMax) {
            newElement.addClass("valid");
        } else {
            newElement.addClass("invalid");
        }
        $("#role_occupation_info_panel").append(newElement);
    }
}

function resetAllSelection() {
    $("#machination_primary_selected").attr("data-code","").html("Aucune");
    $("#machination_info_panel .machination_secondary_selected[id!=machination_secondary_selected_template]").remove();
    $("#character_list_tbody tr[id!=character_line_template]").remove();
    $("#incident_list_tbody tr[id!=incident_line_template]").remove();
    $("#character_select option").removeAttr('disabled');
    recalculateRoleOccupation();
}

function buildAndVisualizeAll() {
    var allHtml = "";
    for( var scenarioIndex = 0 ; scenarioIndex < existingScenarioList.length ; scenarioIndex++) {
        var scenarioData = existingScenarioList[scenarioIndex];
        importerFromScenarioData(scenarioData);
        buildAndVisualize();
        allHtml = allHtml.concat($("#root_display").html(), '<div class="pagebreak"></div>');
        $('#root_display').hide();
        $('#root_edition').show();
    }
    $("#root_display").empty();
    //console.log("allHtml", allHtml);
    $("#root_display").html(allHtml);
    $('#root_edition').hide();
    $('#root_display').show();
}

function buildAndVisualize() {
    $("#root_display").empty();
    $("#root_display").html($("#root_display_template").html());
    var tragedySetCode = $("#tragedy_set_select").val();
    var tragedySetData = mainData.tragedy_sets[tragedySetCode];
    // Scénario
    {
        $("#root_display .scenario-title").html(
            $("#scenario_info_panel input[name=origine_id]").val()
            + ' - '
            + $("#scenario_info_panel input[name=scenario_title]").val()
        );
        $("#root_display .scenario-tragedy-set").html(tragedySetData["name"]);
        var scenarioInfo = "";
        scenarioInfo += "<h4>Informations du Recueil</h4>";
        scenarioInfo += "<p>" +tragedySetData["description"] + "</p>";
        scenarioInfo += $("#scenario_info_panel textarea[name=scenario_informations]").val();
        var mastermindVictoryConditions = $("#scenario_info_panel textarea[name=scenario_mastermind_victory_conditions]").val();
        if("" !== mastermindVictoryConditions ) {
            var mastermindVictoryConditionsHtml = "";
            mastermindVictoryConditionsHtml += "<h4>Conditions de victoire pour le Mastermind</h4>";
            mastermindVictoryConditionsHtml += mastermindVictoryConditions;
            scenarioInfo += mastermindVictoryConditionsHtml;
            var turnInfoPanelElement = $("#root_display .turn_info_panel");
            turnInfoPanelElement.append($("<p></p>").html(mastermindVictoryConditionsHtml));
        }
        $("#root_display .scenario-informations").html(
            scenarioInfo
        );
    }
    // Machination primaire
    {
        var machinationPrimaryCode = $("#machination_info_panel #machination_primary_selected").attr("data-code");
        var machinationPrimary = tragedySetData["machination_primary"][machinationPrimaryCode];
        $("#root_display .machination_info_panel .machination_primary_name").text(machinationPrimary["name"]);
    }
    // Machination(s) secondaire(s)
    {
        $("#machination_info_panel .machination_secondary_selected[id!=machination_secondary_selected_template]")
            .each(function(index, element) {
            var machinationSecondaryCode = $(element).attr("data-code");
            var machinationSecondary = tragedySetData["machination_secondary"][machinationSecondaryCode];
            var newMachinationSecondaryElement = $("#root_display .machination_secondary_display_template").clone();
            newMachinationSecondaryElement.appendTo("#root_display .machination_info_panel")
                .show()
                .removeClass("machination_secondary_display_template");
            newMachinationSecondaryElement.find(".machination_secondary_name")
                .html(machinationSecondary["name"])
                ;
        });
    }
    // Personnages
    {
        $("#character_list_tbody tr[id!=character_line_template]").each(function(index, element){
            var characterCode = $(element).attr("data-code");
            var characterData = mainData.character_list[characterCode];
            var roleCode = $(element).find(".character_role_select").val();
            var additionnalFieldVal = $(element).find(".additionnal_field").val();
            var roleName = "Individu";
            var roleData = {};
            if("" !== roleCode) {
                roleData = tragedySetData["role_list"][roleCode];
                roleName = roleData["name"];
            }
            var newCharacterElement = $("#root_display .character_line_display_template").clone()
                .appendTo("#root_display .character_list_tbody")
                .show()
                .removeClass("character_line_display_template");
            newCharacterElement.find(".character_image").addClass(characterData["image_css_class"]);
            var characterRoleElement = newCharacterElement.find(".character_role");
            {
                var characterRoleHtml = characterData["name"] + "<br/>" + roleName;
                $("#incident_list_tbody").find("tr[id!=incident_line_template]")
                .each(function(index, element) {
                    var incidentCode = $(element).attr("data-code");
                    var jourNumero = $(element).find(".incident_jour").attr("data-jour-numero");
                    var coupable = $(element).find(".incident_coupable_select").val();
                    if(characterCode===coupable) {
                        var incidentData = tragedySetData["incident_list"][incidentCode];
                        characterRoleHtml += "<br/>Jour " + jourNumero + " : " + incidentData["name"];
                    }
                });
                characterRoleHtml += "<br/>";
                characterRoleHtml += "(PL:" + characterData["paranoya_limit"] + ")";
                characterRoleHtml += "Tags : <ul>";
                for(var characterTag in characterData["tags"]) {
                    characterRoleHtml += "<li>" + mainData.character_tag_list[characterData["tags"][characterTag]] + "</li>";
                }
                characterRoleHtml += "</ul>";
                
                characterRoleElement.html(characterRoleHtml);
            }
            var characterInfoElement = newCharacterElement.find(".character_info");
            {
                var characterInfoHtml = "";
                if( undefined !== additionnalFieldVal && "" !== additionnalFieldVal ) {
                    characterInfoHtml += additionnalFieldVal + "<br/>";
                }
                if(
                    (undefined!==roleData["refus_faveur"] && ""!==roleData["refus_faveur"])
                    || 0 !== characterData["faveurs"].length
                ) {
                    characterInfoHtml += "Faveurs";
                    if(undefined!==roleData["refus_faveur"] && ""!==roleData["refus_faveur"]) {
                        characterInfoHtml += " (Refus : "+roleData["refus_faveur"]+")";
                    }
                    characterInfoHtml += "<br/>";
                    if(0 !== characterData["faveurs"].length) {
                        characterInfoHtml += "<ul>";
                        for(var faveurKey in characterData["faveurs"]) {
                            characterInfoHtml += "<li>";
                            var faveurData = characterData["faveurs"][faveurKey];
                            characterInfoHtml += faveurData["cost"] + " : " + faveurData["effect"];
                            characterInfoHtml += "</li>";
                        }
                        characterInfoHtml += "</ul>";
                    }
                }
                if(0 !== characterData["additionnal_rule_list"].length) {
                    var hasPassif = false;
                    for(var arIndex in characterData["additionnal_rule_list"]) {
                        var arData = characterData["additionnal_rule_list"][arIndex];
                        if("passif" === arData["step"]) {
                            hasPassif = true;
                            break;
                        }
                    }
                    if(hasPassif) {
                        characterInfoHtml += "Passif<ul>";
                        for(var arIndex in characterData["additionnal_rule_list"]) {
                            var arData = characterData["additionnal_rule_list"][arIndex];
                            characterInfoHtml += "<li>";
                            characterInfoHtml += arData["description"];
                            characterInfoHtml += "</li>";
                        }
                        characterInfoHtml += "</ul>";
                    }
                }
                if(
                    (
                        undefined !== roleData["capacites"]
                        && 0 !== roleData["capacites"].length
                        )
                    || (
                        undefined !== roleData["description"]
                        && "" !== roleData["description"]
                        )
                    ) {
                    characterInfoHtml += "Capacités :";
                    characterInfoHtml += "<ul>";
                    if(undefined !== roleData["description"]
                        && "" !== roleData["description"]) {
                        characterInfoHtml += "<li>";
                        characterInfoHtml += roleData["description"];
                        characterInfoHtml += "</li>";
                    }
                    for(var capaciteKey in roleData["capacites"]) {
                        characterInfoHtml += "<li>";
                        var capaciteData = roleData["capacites"][capaciteKey];
                        var stepName = mainData["game_step_list"][capaciteData["step"]];
                        characterInfoHtml += "[" + stepName + " : " + capaciteData["condition"] + "] " + capaciteData["description"];
                        characterInfoHtml += "</li>";
                    }
                    characterInfoHtml += "</ul>";
                }
                characterInfoElement.html(characterInfoHtml);
            }
        });
    }
    // Incidents
    {
        $("#incident_list_tbody").find("tr[id!=incident_line_template]")
        .each(function(index, element) {
            var incidentCode = $(element).attr("data-code");
            var jourNumero = $(element).find(".incident_jour").attr("data-jour-numero");
            var coupable = $(element).find(".incident_coupable_select").val();
            var incidentData = tragedySetData["incident_list"][incidentCode];
            var newIncidentElement = $("#root_display .incident_line_template").clone()
                .appendTo("#root_display .incident_list_tbody")
                .show()
                .removeClass("incident_line_template");
            newIncidentElement.find(".incident_jour").text(jourNumero);
            newIncidentElement.find(".incident_name").text(incidentData["name"]);
            newIncidentElement.find(".incident_description").text(incidentData["description"]);
            var characterElement = $("#character_list_tbody tr[id!=character_line_template][data-code='"+coupable+"']");
            var coupabletext = characterElement.find(".character_name").text();
            coupabletext += " ("+characterElement.find(".character_role_select option:selected").text()+")";
            newIncidentElement.find(".incident_coupable").text(coupabletext);
        });
    }
    // Tours de jeu
    {
        var stepDict = {};
        for(var gameStepCode in mainData["game_step_list"]) {
            stepDict[gameStepCode] = [];
        }
        //console.log("stepDict", stepDict);
        // Machination primaire
        {
            var machinationPrimaryCode = $("#machination_info_panel #machination_primary_selected").attr("data-code");
            var machinationPrimary = tragedySetData["machination_primary"][machinationPrimaryCode];
            for(var additionnalRuleKey in machinationPrimary["additionnal_rule_list"]) {
                var additionnalRule = machinationPrimary["additionnal_rule_list"][additionnalRuleKey];
                stepDict[additionnalRule["step"]].push("["+additionnalRule["condition"]+"] "+additionnalRule["description"]);
            }
        }
        // Machination(s) secondaire(s)
        {
            $("#machination_info_panel .machination_secondary_selected[id!=machination_secondary_selected_template]")
                .each(function(index, element) {
                var machinationSecondaryCode = $(element).attr("data-code");
                var machinationSecondary = tragedySetData["machination_secondary"][machinationSecondaryCode];
                for(var additionnalRuleKey in machinationSecondary["additionnal_rule_list"]) {
                    var additionnalRule = machinationSecondary["additionnal_rule_list"][additionnalRuleKey];
                    stepDict[additionnalRule["step"]].push("["+additionnalRule["condition"]+"] "+additionnalRule["description"]);
                }
            });
        }
        // Personnages + Rôles
        {
            $("#character_list_tbody tr[id!=character_line_template]").each(function(index, element){
                // Personnage
                var characterCode = $(element).attr("data-code");
                var characterData = mainData.character_list[characterCode];
                
                for(var additionnalRuleKey in characterData["additionnal_rule_list"]) {
                    var additionnalRule = characterData["additionnal_rule_list"][additionnalRuleKey];
                    var additionnalRuleText = "["+characterData["name"];
                    if(
                        undefined !== additionnalRule["condition"]
                        && "" !== additionnalRule["condition"]
                    ) {
                        additionnalRuleText += ", " + additionnalRule["condition"];
                    }
                    additionnalRuleText += "] " + additionnalRule["description"];
                    stepDict[additionnalRule["step"]].push(additionnalRuleText);
                }
                // Rôle
                var roleCode = $(element).find(".character_role_select").val();
                if("" !== roleCode) {
                    var roleData = tragedySetData["role_list"][roleCode];
                    var roleName = roleData["name"];
                    if(
                        undefined !== roleData["capacites"]
                        && 0 !== roleData["capacites"].length
                    ) {
                        for(var additionnalRuleKey in roleData["capacites"]) {
                            var additionnalRule = roleData["capacites"][additionnalRuleKey];
                            var additionnalRuleText = "["+characterData["name"];
                            additionnalRuleText += " (" + roleName + ")";
                            if(
                                undefined !== additionnalRule["condition"]
                                && "" !== additionnalRule["condition"]
                            ) {
                                additionnalRuleText += ", " + additionnalRule["condition"];
                            }
                            additionnalRuleText += "] " + additionnalRule["description"];
                            stepDict[additionnalRule["step"]].push(additionnalRuleText);
                        }
                    }
                }
            });
        }
        // Incidents
        // TODO ?
        for(var stepCode in stepDict) {
            if(0!==stepDict[stepCode]) {
                var stepText = "<ul>";
                for(var additionnalRuleKey in stepDict[stepCode]) {
                    stepText += "<li>";
                    stepText += stepDict[stepCode][additionnalRuleKey];
                    stepText += "</li>";
                }
                stepText += "</ul>";
                var stepElement = $("#root_display .turn_info_panel [data-code='"+stepCode+"']");
                stepElement.append(stepText);
            }
        }
    }
    $('#root_edition').hide();
    $("#root_display").show();
}

function exporterToTextarea() {
    var textareaElement = $("textarea[name='export_import_textarea']").first();
    var scenarioData = {};
    var tragedySetCode = $("#tragedy_set_select").val();
    scenarioData["tragedySet"] = tragedySetCode;
    scenarioData["title"] = $("#scenario_info_panel input[name=scenario_title]").val();
    scenarioData["origine_id"] = $("#scenario_info_panel input[name=origine_id]").val();
    scenarioData["origine_tag"] = $("#scenario_info_panel input[name=origine_tag]").val();
    scenarioData["origine"] = $("#scenario_info_panel input[name=origine]").val();
    scenarioData["informations"] = $("#scenario_info_panel textarea[name=scenario_informations]").val();
    scenarioData["mastermind_victory_conditions"] = $("#scenario_info_panel textarea[name=scenario_mastermind_victory_conditions]").val();
    // Machination principale
    scenarioData["machination_primary"] = $("#machination_info_panel #machination_primary_selected").attr("data-code");
    // Machination(s) secondaire(s)
    scenarioData["machination_secondary_list"] = [];
    {
        $("#machination_info_panel .machination_secondary_selected[id!=machination_secondary_selected_template]")
            .each(function(index, element) {
            var machinationSecondaryCode = $(element).attr("data-code");
            scenarioData["machination_secondary_list"].push(machinationSecondaryCode);
        });
    }
    // Personnages
    scenarioData["character_list"] = [];
    {
        $("#character_list_tbody tr[id!=character_line_template]").each(function(index, element){
            var characterCode = $(element).attr("data-code");
            var roleCode = $(element).find(".character_role_select").val();
            var additionnalFieldVal = $(element).find(".additionnal_field").val();
            var characterData = {};
            characterData["characterCode"] = characterCode;
            characterData["roleCode"] = roleCode;
            if( undefined !== additionnalFieldVal && "" !== additionnalFieldVal ) {
                characterData["additionnal_field"] = additionnalFieldVal;
            }
            scenarioData["character_list"].push(characterData);
        });
    }
    // Incidents
    scenarioData["incident_list"] = [];
    {
        $("#incident_list_tbody").find("tr[id!=incident_line_template]")
        .each(function(index, element) {
            var incidentCode = $(element).attr("data-code");
            var jourNumero = $(element).find(".incident_jour").attr("data-jour-numero");
            var coupable = $(element).find(".incident_coupable_select").val();
            var incidentData = {};
            incidentData["incidentCode"] = incidentCode;
            incidentData["jourNumero"] = jourNumero;
            incidentData["coupable"] = coupable;
            scenarioData["incident_list"].push(incidentData);
        });
    }
    textareaElement.val(JSON.stringify(scenarioData, null, 4));
}

function importerFromTextarea() {
    var textareaElement = $("textarea[name='export_import_textarea']").first();
    if(""===textareaElement.val()) {
        console.log("Can't load empty scenario");
        return;
    }
    var scenarioData = JSON.parse(textareaElement.val());
    importerFromScenarioData(scenarioData);
    textareaElement.val("");
}

function importerFromScenarioData(scenarioData) {
    $("#tragedy_set_select").val(scenarioData["tragedySet"]);
    loadNewTragedySet();
    $("#scenario_info_panel input[name=scenario_title]").val(scenarioData["title"]);
    $("#scenario_info_panel input[name=origine_id]").val(scenarioData["origine_id"]);
    $("#scenario_info_panel input[name=origine_tag]").val(scenarioData["origine_tag"]);
    $("#scenario_info_panel input[name=origine]").val(scenarioData["origine"]);
    $("#scenario_info_panel textarea[name=scenario_informations]").val(scenarioData["informations"]);
    $("#scenario_info_panel textarea[name=scenario_mastermind_victory_conditions]").val(scenarioData["mastermind_victory_conditions"]);
    
    
    // Machination principale
    {
        $("#machination_primary_select").val(scenarioData["machination_primary"]);
        setMachinationPrimary();
    }
    // Machination(s) secondaire(s)
    {
        for(var machinationSecondaryKey in scenarioData["machination_secondary_list"]) {
            var machinationSecondaryCode = scenarioData["machination_secondary_list"][machinationSecondaryKey];
            $("#machination_secondary_select").val(machinationSecondaryCode);
            addMachinationSecondary();
        }
    }
    // Personnages
    {
        for(var characterDataKey in scenarioData["character_list"]) {
            var characterData = scenarioData["character_list"][characterDataKey];
            var characterCode = characterData["characterCode"];
            var roleCode = characterData["roleCode"];
            var additionnalFieldVal = characterData["additionnal_field"];
            $("#character_select").val(characterCode);
            var newCharacterElement = addCharacter();
            newCharacterElement.find("select.character_role_select").val(roleCode);
            if( undefined !== additionnalFieldVal && "" !== additionnalFieldVal ) {
                newCharacterElement.find(".additionnal_field").val(additionnalFieldVal);
            }
        }
    }
    // Incidents
    {
        for(var incidentDataKey in scenarioData["incident_list"]) {
            var incidentData = scenarioData["incident_list"][incidentDataKey];
            var incidentCode = incidentData["incidentCode"];
            var jourNumero = incidentData["jourNumero"];
            var coupable = incidentData["coupable"];
            $("#incident_select").val(incidentCode);
            $("#incident_day_input").val(jourNumero);
            
            $("#character_select").val(characterCode);
            var newIncidentElement = addIncident();
            newIncidentElement.find("select.incident_coupable_select").val(coupable);
        }
    }
    recalculateRoleOccupation();
    checkIncidentValidity();
}
