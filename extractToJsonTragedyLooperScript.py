import time
import requests
import json
from urllib.parse import urlparse
from bs4 import BeautifulSoup
from pathlib import Path

conversionMap = {
	"tragedySet":{
		"First Steps":"first_steps",
		"Basic Tragedy":"base_tragedy"
	},
	"mainPlot":{
		# "A Place to Protect":"meurtre_premedite",
		# "A Place to Protect":"lumiere_vengeresse",
		"A Place to Protect":"un_lieu_a_proteger",
		"The Sealed Item":"l_objet_scelle",
		"Sign with me":"choisis_moi",
		"Sign with me!":"choisis_moi",
		"Change of Future":"futur_boulverse",
		"Giant Time bomb":"bombe_temporelle"
	},
	"subPlot":{
		# "Threads of Fate":"l_ombre_de_leventreur",
		"An Unsettling Rumor":"rumeur_troublante",
		"Unsettling Rumor":"rumeur_troublante",
		"A Hideous Script":"un_script_terrifiant",
		": Circle of friends":"cercle_d_amis",
		"Circle of Friends":"cercle_d_amis",
		"A Love Affair":"histoire_d_amour",
		"The Hidden freak":"obsede_cache",
		"The Hidden Freak":"obsede_cache",
		"Parinoia virus":"paranoia_mortelle",
		"Paranoia Virus":"paranoia_mortelle",
		"Threads of Fate":"fils_du_destin",
		"Unknown Factor X":"element_inconnu_x"
	},
	"characterName":{
		"Pop Idol":"pop_star",
		"Alien":"extraterrestre",
		"Nurse":"infirmiere",
		"Hospital patient":"patient",
		"Patient":"patient",
		"Godly being":"etre_divin",
		"Godly Being":"etre_divin",
		"Office Worker":"employe_de_bureau",
		"Officer Worker":"employe_de_bureau",
		"Henchman":"sbire",
		"Informer":"informatrice",
		"Informant":"informatrice",
		"Doctor":"medecin",
		"Shrine maiden":"gardienne_du_sanctuaire",
		"Shrine Maiden":"gardienne_du_sanctuaire",
		"Journalist":"journaliste",
		"Mystery Boy":"garcon_mysterieux",
		"Boy Student":"lyceen",
		"Rich Man's Daughter":"riche_heritiere",
		"Rich mans daughter":"riche_heritiere",
		"Rich Man’s Daughter":"riche_heritiere",
		"Girl Student":"lyceenne",
		"Class Rep":"deleguee_de_classe",
		"Police Officer":"policier"
	},
	"characterRole":{
		"Key Person":"personnage_cle",
		"Killer":"tueur",
		"Serial Killer":"tueur_en_serie",
		"Lover":"amoureux",
		"Loved One":"etre_aime",
		"Witch":"sorciere",
		"Conspiracy Theorist":"conspirationniste",
		"Brain":"cerveau",
		"Cultist":"cultiste",
		"Friend":"ami",
		"Curmudgeon":"grincheux",
		"Time Traveler":"voyageur_temporel",
		"Time Traveller":"voyageur_temporel",
		# "Factor":"agent_dormant",
		"":"",
		"Person":""
	},
	"incidentName":{
		"Murder":"meurtre",
		"Increasing Unease":"malaise_grandissant",
		"Unsettling Rumor":"malaise_grandissant",
		"Suicide":"suicide",
		"Hospital Incident":"incident_a_l_hopital",
		"Distant Murder":"meurtre_distant",
		"Missing Person":"personne_disparue",
		# "Missing Person":"contagion",
		# "Foul Evil":"mal_odieux",
		"Butterfly Effect":"effet_papillon",
		# "Butterfly Effect":"presage",
		# "Butterfly Effect":"terrorisme",
		# "Butterfly Effect":"meurtre_bestial",
		"Foul Evil":"mal_odieux"
	}
}
for keyType in conversionMap:
	conversionMapCopy = conversionMap[keyType].copy()
	for keyConversion in conversionMap[keyType]:
		conversionMapCopy[keyConversion.lower()] = conversionMapCopy[keyConversion]
	conversionMap[keyType] = conversionMapCopy

missingConversion = dict()
# indexInError = []
indexInError = [6,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,45,47,48,49,50,51,52,53,54,55,56,57,58,59,60,61,62,64,69,71,75,79,81,130,160,168,171,172,175,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199]

hasConversionError = False
def getConversion(mapType, inputKey):
	inputKey = inputKey.strip()
	convertedData = inputKey
	if mapType in conversionMap:
		if inputKey.lower() in conversionMap[mapType]:
			convertedData = conversionMap[mapType][inputKey.lower()]
		else:
			print(f"{inputKey} not present in conversionMap[{mapType}]")
			hasConversionError = True
			if mapType not in missingConversion:
				missingConversion[mapType] = []
			if inputKey not in missingConversion[mapType]:
				missingConversion[mapType].append(inputKey)
	else:
		print(f"{mapType} is not a valid mapType")
		hasConversionError = True
	return convertedData

def getHeaders(url):
	return {
		"Referer": url,
		"User-Agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:91.0) Gecko/20100101 Firefox/91.0",
		"Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
		"Accept-Language": "fr,fr-FR;q=0.8,en-US;q=0.5,en;q=0.3",
		"Upgrade-Insecure-Requests": "1",
		"Sec-Fetch-Dest": "document",
		"Sec-Fetch-Mode": "navigate",
		"Sec-Fetch-Site": "same-origin",
		"Sec-Fetch-User": "?1",
		"Pragma": "no-cache",
		"Cache-Control": "no-cache"
	}

def getdata(url):
	r = requests.get(url, headers=getHeaders(url))
	if r.status_code != 200:
		raise requests.HTTPError(f'Url "{url}" did not respond (status: {r.status_code}).')
	return r.text
	
def getJsonFromUrl(url):
	outputData = {}
	dataForReplacement = {
		"creator": "",
		"nb_loops": "",
		"difficulty": "",
		"nb_days": "",
		"special_rules": "",
		"story": "",
		"hints": ""
	}
	# print("url : ", url)
	htmldata = getdata(url)
	soup = BeautifulSoup(htmldata, 'html.parser')
	for item in soup.find_all('table'):
		# print("item :", item)
		itemFirstColumnHeadText = item.find('thead').find('tr').find('th').get_text()
		# print("itemFirstColumnHeadText :", itemFirstColumnHeadText)
		if itemFirstColumnHeadText == "Cast":
			# outputData["title"] = itemContentText
			characters = item.find('tbody').find_all('tr')
			# print("characters :", characters)
			for c in characters:
				ccol = c.find_all('td')
				character_name = ccol[0].get_text().strip(' :')
				character_role = ccol[1].get_text().strip(' :')
				# Character name conversion
				character_name = getConversion("characterName", character_name)
				# Character role conversion
				character_role = getConversion("characterRole", character_role)
				if "character_list" not in outputData:
					outputData["character_list"] = []
				outputData["character_list"].append({
					"characterCode": character_name,
					"roleCode": character_role
				})
		elif itemFirstColumnHeadText == "Day":
			# outputData["tragedySet"] = itemContentText
			incidents = item.find('tbody').find_all('tr')
			# print("incidents :", incidents)
			for inci in incidents:
				icol = inci.find_all('td')
				incident_day = icol[0].get_text()[-1:]
				incident_name = icol[1].get_text().strip(' :')
				incident_coupable = icol[2].get_text().strip(' :')
				if "-" != incident_name:
					# Incident name conversion
					incident_name = getConversion("incidentName", incident_name)
					# Character name conversion
					incident_coupable = getConversion("characterName", incident_coupable)
					if "incident_list" not in outputData:
						outputData["incident_list"] = []
					outputData["incident_list"].append({
						"incidentCode": incident_name,
						"jourNumero": incident_day,
						"coupable": incident_coupable
					})
	for item in soup.find_all('p'):
		itemLabel = item.find('strong')
		if not itemLabel:
			continue
		itemLabelText = itemLabel.get_text().strip(' :')
		# print("item : ", item)
		itemContentText = ""
		for sibling in itemLabel.next_siblings:
			# print("sibling : ", sibling)
			try:
				itemContentText = itemContentText + sibling.strip()
			except TypeError:
				# print("Error sibling")
				continue
		if itemLabelText == "Title":
			outputData["title"] = itemContentText
		elif itemLabelText == "Tragedy Set":
			# Tragedy Set conversion
			itemContentText = getConversion("tragedySet", itemContentText)
			outputData["tragedySet"] = itemContentText
		elif itemLabelText == "Main Plot":
			# Main plot conversion
			itemContentText = getConversion("mainPlot", itemContentText)
			outputData["machination_primary"] = itemContentText
		elif itemLabelText == "Subplot 1":
			if "machination_secondary_list" not in outputData:
				outputData["machination_secondary_list"] = []
			# Sub plot conversion
			itemContentText = getConversion("subPlot", itemContentText)
			outputData["machination_secondary_list"].append(itemContentText)
		elif itemLabelText == "Subplot 2":
			if itemContentText.strip():
				if "machination_secondary_list" not in outputData:
					outputData["machination_secondary_list"] = []
				# Sub plot conversion
				itemContentText = getConversion("subPlot", itemContentText)
				outputData["machination_secondary_list"].append(itemContentText)
		elif itemLabelText == "Victory Conditions for Mastermind":
			outputData["mastermind_victory_conditions"] = itemContentText
		elif itemLabelText == "Created by":
			dataForReplacement["creator"] = itemContentText
		elif itemLabelText == "Number of Loops":
			dataForReplacement["nb_loops"] = itemContentText
		elif itemLabelText == "Difficulty":
			dataForReplacement["difficulty"] = itemContentText
		elif itemLabelText == "Special Rule":
			dataForReplacement["special_rules"] = itemContentText
		elif itemLabelText == "Days per Loop":
			dataForReplacement["nb_days"] = itemContentText
		elif itemLabelText == "Description":
			dataForReplacement["story"] = itemContentText
		elif itemLabelText == "Hints for Mastermind":
			dataForReplacement["hints"] = itemContentText
		elif itemLabelText == "Comments":
			if itemContentText:
				print("Commentaires : ", itemContentText)
		else:
			print("itemLabelText : ", itemLabelText)
			print("itemContentText : ", itemContentText)
	outputData["origine_tag"] = "TLS"
	outputData["origine"] = "Tragedylooperscripts.com"
	outputData["informations"] = f"<p>\n<strong>Créateur</strong> : {dataForReplacement['creator']}<br />\n<strong>Boucles</strong> : {dataForReplacement['nb_loops']} / <strong>Difficulté</strong> : {dataForReplacement['difficulty']}<br />\n<strong>Jours par boucle</strong> : {dataForReplacement['nb_days']}\n</p>\n<h4>Particularités</h4>\n<p>\n{dataForReplacement['special_rules']}\n</p>\n<h4>Histoire</h4>\n<p>\n{dataForReplacement['story']}\n</p>\n<h4>Conseils pour le Mastermind</h4>\n<p>\n{dataForReplacement['hints']}\n</p>"
	return outputData

def extractScripts(startingIndex, endingIndex):
	dataArray = []
	for index in range(startingIndex,endingIndex):
		if index in indexInError:
			continue
		try:
			hasConversionError = False
			dataFromUrl = getJsonFromUrl(f"http://www.tragedylooperscripts.com/scripts/{index}/mastermind")
			dataFromUrl["origine_id"] = str(index)
			dataFromUrl["hasError"] = hasConversionError
			# print("data : ", dataFromUrl)
			dataArray.append(dataFromUrl)
			time.sleep(1)
		except TypeError as ex:
			print(ex)
			print(f"Breaking loop with index {index}")
			break
		except requests.HTTPError as ex:
			print(ex)
			indexInError.append(index)
			# print("Breaking loop")
			# break
	# getJsonFromUrl("http://www.tragedylooperscripts.com/scripts/177/mastermind")
	return dataArray

if __name__ == "__main__":
	dataArray = extractScripts(1, 200)
	# print("dataArray :", json.dumps(dataArray, indent=4))
	print("missingConversion :", json.dumps(missingConversion, indent=4))
	# print("indexInError :", json.dumps(indexInError, indent=4))
	with open('missingConversion.json', 'w') as f:
		f.write(json.dumps(missingConversion, indent=4))
	with open('output.json', 'w') as f:
		f.write(json.dumps(dataArray, indent=4))
	noErrorDataArray = []
	for data in dataArray:
		if not data["hasError"]:
			noErrorDataArray.append(data)
	with open('noErrorOutput.json', 'w') as f:
		f.write(json.dumps(noErrorDataArray, indent=4))
