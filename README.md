# Roadmap

* Ajouter la possibilité de préparer une impression de tous les scénarios enregistrés
* Ajouter les effets/règles particulières des machinations actives dans la liste des machinations
* Préparer une localisation de la page
* Ajouter les recueils de tragédie
    * Prime Evil
    * Cosmic Mythology
    * Haunted Stage
    * Another Horizon
* Ajouter les scripts
    * Mystery circle
    * Midnight Zone
    * Prime Evil
    * Cosmic Mythology
    * Haunted Stage
    * TragedyLooperScripts.com (Faire un parser automatique pour récupérer et mapper les éléments pour import automatisé)

# Bugs to fix

* Rien pour l'instant

# Accès en ligne

Hébergement gitlab.io : https://messu.gitlab.io/tragedylooperscripthelpcreator/

# Remerciements/Thanks

* BakaFire pour la création du jeu Tragedy Looper.
* Z-man pour lédition anglaise du jeu.
* Filosofia Éditions (disparue depuis) pour l'édition française du jeu.
* Hav0k, Soragons et Zakuro Uzueda pour leurs conseils et leur aide pour récupérer les données.
* McQueen pour sa traduction disponible sur BGG des recueils des extensions.

